/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading;
using log4net;
using OpenMetaverse;
using OpenSim.Framework;
using MySql.Data.MySqlClient;

namespace OpenSim.Data.MySQL
{
	/// <summary>
	/// A MySQL Interface for managing grid bans
	/// </summary>
	public class MySQLBansServiceData : IBansServiceData
	{
		// private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private MySQLBansServiceAgentHandler m_Agents;
		private MySQLBansServiceHardwareHandler m_Hardware;
		private MySQLBansServiceIPHandler m_IPs;

		public MySQLBansServiceData(string connectionString) 
		{
			Assembly assem = GetType().Assembly;
			using (MySqlConnection dbcon = new MySqlConnection(connectionString))
			{
				dbcon.Open();
				Migration m = new Migration(dbcon, assem, "BansStore");
				m.Update();
			}

			m_Agents = new MySQLBansServiceAgentHandler(connectionString, "banned_agents", "banned_agents");
			m_Hardware = new MySQLBansServiceHardwareHandler(connectionString, "banned_hardware", "banned_hardware");
			m_IPs = new MySQLBansServiceIPHandler(connectionString, "banned_ips", "banned_ips");
		}

		public bool IsBanned(string userID, string ip, string id0, string origin)
		{
			return (m_Agents.getAgent(userID).Length > 0 ||
				m_Hardware.getHardware(id0).Length > 0 ||
				m_IPs.getIP(ip).Length > 0);
		}

		public bool BanAgent(UUID id, string reason)
		{
			BanDict ban = new BanDict();
			ban.Data["UserID"] = id.ToString();
			ban.Data["Reason"] = reason;
			return m_Agents.Store(ban);
		}

		public bool BanId0(string id, string reason)
		{
			BanDict ban = new BanDict();
			ban.Data["HardwareID"] = id;
			ban.Data["Reason"] = reason;
			return m_Hardware.Store(ban);
		}

		public bool BanIP(string id, string reason)
		{
			BanDict ban = new BanDict();
			ban.Data["IPAddress"] = id;
			ban.Data["Reason"] = reason;
			return m_IPs.Store(ban);
		}
	}

	#region table handlers

	public class BanDict
	{
		public Dictionary<string, string> Data;
	}

	public class MySQLBansServiceAgentHandler : MySQLGenericTableHandler<BanDict>
	{
		public MySQLBansServiceAgentHandler(string connectionString, string realm, string store) 
			: base(connectionString, realm, store)
		{
		}

		public BanDict[] getAgent(string id)
		{
			return Get("UserID", id);
		}
			
	}

	public class MySQLBansServiceHardwareHandler : MySQLGenericTableHandler<BanDict>
	{
		public MySQLBansServiceHardwareHandler(string connectionString, string realm, string store) 
			: base(connectionString, realm, store)
		{
		}

		public BanDict[] getHardware(string id)
		{
			return Get("HardwareID", id);
		}
	}

	public class MySQLBansServiceIPHandler : MySQLGenericTableHandler<BanDict>
	{
		public MySQLBansServiceIPHandler(string connectionString, string realm, string store) 
			: base(connectionString, realm, store)
		{
		}

		public BanDict[] getIP(string id)
		{
			return Get("IPAddress", id);
		}
	}
	#endregion
}
