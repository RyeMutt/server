﻿/*
 * Copyright (C) 2015, Cinder Roxley <cinder@sdf.org>
 * 
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 * 
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using log4net;
using Mono.Addins;
using Nini.Config;
using OpenMetaverse;
using OpenSim.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using OpenSim.Server.Base;
using OpenSim.Services.Interfaces;

namespace OpenSim.Region.CoreModules.ServiceConnectorsOut.UserLog
{
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "LocalUserLogServicesConnector")]
    public class LocalUserLogServiceConnector : ISharedRegionModule, IUserLogService
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IUserLogService m_UserLogService;
        private bool m_Enabled = false;

        #region ISharedRegionModule

        public Type ReplaceableInterface 
        {
            get { return null; }
        }

        public string Name
        {
            get { return "LocalUserLogServicesConnector"; }
        }

        public void Initialise(IConfigSource source)
        {
            IConfig moduleConfig = source.Configs["Modules"];
            if (moduleConfig != null)
            {
                string name = moduleConfig.GetString("UserLogServices", "");
                if (name == Name)
                {
                    IConfig userConfig = source.Configs["UserLogService"];
                    if (userConfig == null)
                    {
                        m_log.Error("[USER LOG CONNECTOR]: UserLogService missing from OpenSim.ini");
                        return;
                    }

                    string serviceDll = userConfig.GetString("LocalServiceModule", String.Empty);

                    if (String.IsNullOrEmpty(serviceDll))
                    {
                        m_log.Error("[USER LOG CONNECTOR]: No UserLogModule named in section UserLogService");
                        return;
                    }

                    Object[] args = new Object[] { source };
                    m_UserLogService = ServerUtils.LoadPlugin<IUserLogService>(serviceDll, args);

                    if (m_UserLogService == null)
                    {
                        m_log.Error("[USER LOG CONNECTOR]: Can't load user log service");
                        return;
                    }
                    m_Enabled = true;
                    m_log.Info("[USER LOG CONNECTOR]: Local user log connector enabled");
                }
            }
        }

        public void PostInitialise()
        {
            if (!m_Enabled)
                return;
        }

        public void Close()
        {
            if (!m_Enabled)
                return;
        }

        public void AddRegion(Scene scene)
        {
            if (!m_Enabled)
                return;

            scene.RegisterModuleInterface<IUserLogService>(this);
        }

        public void RemoveRegion(Scene scene)
        {
            if (!m_Enabled)
                return;
        }

        public void RegionLoaded(Scene scene)
        {
            if (!m_Enabled)
                return;
        }

        #endregion ISharedRegionModule

        #region IUserLogService

        public void RecordAgentLogin(UUID AgentID, string Name, IPAddress IP, string MACAddress, 
            string Id0, string Viewer, string Platform, string PlatformVersion, string Grid)
        {
            m_UserLogService.RecordAgentLogin(AgentID, Name, IP, MACAddress, Id0, Viewer, Platform, PlatformVersion, Grid);
        }

        public void RecordViewerStats(UUID AgentID, string Stats)
        {
            m_UserLogService.RecordViewerStats(AgentID, Stats);
        }

        #endregion IUserLogService
    }
}

