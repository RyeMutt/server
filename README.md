# YrGrid Server #

The YrGrid Server is based on the OpenSimulator Project.

Use of the YrGrid Server is governed under the GNU Affero General Public License. Need to borrow some code but this license is incompatible with your project? Please contact cinder@alchemyviewer.org for relicensing information.

    YrGrid Server Virtual World Platform
    Copyright (C) OpenSimulator Contributors (See CONTRIBUTORS.txt for a full list of names)
    Copyright (C) 2015 Cinder Roxley <cinder@sdf.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.