/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using log4net;
using Nini.Config;
using Mono.Addins;
using OpenMetaverse;
using OpenSim.Framework;
using OpenSim.Framework.Servers;
using OpenSim.Framework.Client;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;

namespace OpenSim.Region.CoreModules.Avatar.InstantMessage
{
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "MuteListModule")]
    public class MuteListModule : ISharedRegionModule
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private bool enabled = true;
        private List<Scene> m_SceneList = new List<Scene>();
        private string m_RestURL = String.Empty;

        public void Initialise(IConfigSource config)
        {
            IConfig cnf = config.Configs["Messaging"];
            if (cnf == null)
            {
                enabled = false;
                return;
            }

            if (cnf != null && cnf.GetString("MuteListModule", "None") !=
                    "MuteListModule")
            {
                enabled = false;
                return;
            }

            m_RestURL = cnf.GetString("MuteListURL", "");
            if (m_RestURL == "")
            {
                m_log.Error("[MUTE LIST] Module was enabled, but no URL is given, disabling");
                enabled = false;
                return;
            }
        }

        public void AddRegion(Scene scene)
        {
            if (!enabled)
                return;

            lock (m_SceneList)
            {
                m_SceneList.Add(scene);

                scene.EventManager.OnNewClient += OnNewClient;
            }
        }

        public void RegionLoaded(Scene scene)
        {
        }

        public void RemoveRegion(Scene scene)
        {
            if (!enabled)
                return;

            lock (m_SceneList)
            {
                m_SceneList.Remove(scene);
            }
        }

        public void PostInitialise()
        {
            if (!enabled)
                return;

            m_log.Debug("[MUTE LIST] Mute list enabled");
        }

        public string Name
        {
            get { return "MuteListModule"; }
        }

        public Type ReplaceableInterface
        {
            get { return null; }
        }
        
        public void Close()
        {
        }
       
        private void OnNewClient(IClientAPI client)
        {
            client.OnMuteListRequest += OnMuteListRequest;
			client.OnUpdateMuteListEntry += OnUpdateMuteListEntry; 
			client.OnRemoveMuteListEntry += OnRemoveMuteListEntry;
        }

        private void OnMuteListRequest(IClientAPI client, uint crc)
        {
			int cnt = 0;
			string str = String.Empty;
			string url = m_RestURL + "?method=RequestList";

			List<GridMuteList> mllist = SynchronousRestObjectRequester.MakeRequest<UUID, List<GridMuteList>>("POST", url, client.AgentId);
			while (mllist==null && cnt < 10) {		// retry
				mllist = SynchronousRestObjectRequester.MakeRequest<UUID, List<GridMuteList>>("POST", url, client.AgentId);
				cnt++;
			}

			if (mllist != null) {
				foreach (GridMuteList ml in mllist)
				{
					str += ml.muteType.ToString()+" "+ml.muteID.ToString()+" "+ml.muteName+"|"+ml.muteFlags.ToString()+"\n";
				}
			}
			else 
			{
				m_log.ErrorFormat("[MUTE LIST] Not response from {0}", m_RestURL);
				return;
			}

			string filename = "mutes" + client.AgentId.ToString();
			IXfer xfer = client.Scene.RequestModuleInterface<IXfer>();
			if (xfer != null)
			{
				byte[] byteArray = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(str);
				xfer.AddNewFile(filename, byteArray);
				client.SendMuteListUpdate(filename);
			}
        }

		private void OnUpdateMuteListEntry(IClientAPI client, UUID MuteID, string Name, int Type, UUID AgentID) 
		{
			GridMuteList ml = new GridMuteList(AgentID, MuteID, Name, Type, 0);
			bool success = SynchronousRestObjectRequester.MakeRequest<GridMuteList, bool>("POST", m_RestURL+"?method=UpdateList", ml);
			m_log.DebugFormat("[MUTE LIST] Update entry for {0} was {1}", AgentID, success ? "successful" : "not successful");
		}


		private void OnRemoveMuteListEntry(IClientAPI client, UUID MuteID, string Name, UUID AgentID)
		{
			GridMuteList ml = new GridMuteList(AgentID, MuteID, Name, 0, 0);
			bool success = SynchronousRestObjectRequester.MakeRequest<GridMuteList, bool>("POST", m_RestURL+"?method=DeleteList", ml);
			m_log.DebugFormat("[MUTE LIST] Delete entry for {0} was {1}", AgentID, success ? "successful" : "not successful");
		}
    }

	public class GridMuteList
	{
		public Guid agentID;
		public Guid muteID;
		public string muteName;
		public int  muteType;
		public int  muteFlags;
		public uint timestamp;


		public GridMuteList()
		{ 
		}


		public GridMuteList(UUID _uuid, UUID _mute, string _name, int _type, int _flags)
		{
			agentID	  = _uuid.Guid;
			muteID	  = _mute.Guid;
			muteName  = _name;
			muteType  = _type;
			muteFlags = _flags;
			timestamp = (uint)Util.UnixTimeSinceEpoch();
		}
	}
}
