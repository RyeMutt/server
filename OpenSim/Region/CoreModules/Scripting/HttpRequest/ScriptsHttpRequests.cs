/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Security.Cryptography.X509Certificates;
using log4net;
using Nini.Config;
using OpenMetaverse;
using OpenSim.Framework;
using OpenSim.Framework.Servers;
using OpenSim.Framework.Servers.HttpServer;
using OpenSim.Framework.Monitoring;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using Mono.Addins;

/*****************************************************
 *
 * ScriptsHttpRequests
 *
 * Implements the llHttpRequest and http_response
 * callback.
 *
 * All we're going to do with the new requests is queue them.
 * Another thread will handle the requests in order.  If we
 * queue a request and no such thread is running, we'll start
 * it.  If that thread runs out of requests, it will stay
 * running for a few seconds/minutes before exiting.
 *
 * some notes:
 * HttpRequestModule will handle:
 *  queueing new requests
 *  giving complete responses to Async Command Manager
 *
 * FilteringNameResolver will handle:
 *  Resolving host names and handling blacklists
 *
 * HttpRequestClass will handle:
 *  performing the actual request
 *  parsing the response headers and body
 *  trigger an event on completion
 *
 * Basic flow of data
 *  LSL script calls llHTTPRequest()
 *  XEngine calls HttpRequestModule.StartHttpRequest()
 *  StartHttpRequest will then:
 *      check object throttling
 *      If request is throttled, return NULL_KEY
 *      Add the request to the PendingRequests queue
 *      If a QueueManager thread is not running, one is started
 *
 *  The HttpRequestModule.QueueManager thread will then:
 *      Check if there are worker threads available
 *      If there are, check if there is a queued request
 *      If there is, dequeue it and start a worker thread
 *      If the queue is empty, look at LastRequestStarted, and
 *          if it's been awhile (maybe 10 minutes?) end the thread
 *
 *  The worker thread will then:
 *      Check with FilteringNameResolver.TryResolve()
 *      If TryResolve returns non-null, do the HTTP request work
 *      Trigger an event for RequestFinished
 *
 *  The HttpRequestModule.RequestFinished callback is then invoked and:
 *      Adds the completed request to the FinishedRequests queue
 *      Removes in from the in-process queue
 *
 *  The HttpRequestModule.StopHttpRequestsForScript() method will:
 *      Check the PendingRequests queue and remove any requests for the specified script
 *      There's no reference to the request any more, so we can't call HttpRequest.Stop()
 *      It may be worthwhile to add an InProcessRequests vector or something for this later
 * 
 * TODO:
 *  Proxy support has not been implemented in the new module
 *  HTTP_VERBOSE_THROTTLE is not being used, and throttling messages are never sent
 *  SSL Certificate verification
 *
 */

namespace OpenSim.Region.CoreModules.Scripting.HttpRequest
{
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "HttpRequestModule")]
    public class HttpRequestModule : ISharedRegionModule, IHttpRequestModule
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private LinkedList<HttpRequestClass> PendingRequests = new LinkedList<HttpRequestClass>();
        private ReaderWriterLock PendingLock = new ReaderWriterLock();
        private LinkedList<HttpRequestClass> InProcessRequests = new LinkedList<HttpRequestClass>();
        private ReaderWriterLock InProcessLock = new ReaderWriterLock();
        private LinkedList<HttpRequestClass> FinishedRequests = new LinkedList<HttpRequestClass>();
        private ReaderWriterLock FinishedLock = new ReaderWriterLock();
        private Dictionary<UUID, Queue<DateTime>> RequestTracker = new Dictionary<UUID, Queue<DateTime>>();
        private ReaderWriterLock RequestTrackerLock = new ReaderWriterLock();
        private Thread qmThread = null;
        private ReaderWriterLock QueueManagerLock = new ReaderWriterLock();
        // and config settings //
        private FilteringNameResolver resolver = new FilteringNameResolver();

        // some of these are needed for interface I think
        private int httpTimeout = 60000;
        private string m_name = "HttpScriptRequests";
        
        // stats
        private int statProcessed = 0;
        private int statRequested = 0;
        private int statThrottled = 0;
        private int statThrottled2 = 0;

        // Proxy support may be added later //
        //private string m_proxyurl = "";
        //private string m_proxyexcepts = "";

        // do we need this for something? -> private Scene m_scene;
        private int MaxConcurrentRequests = 50;
        private int MaxQueuedRequests = 500;
        
        public HttpRequestModule()
        {
        }

        private void handleRequestFinished(object sender, EventArgs arg)
        {
            // the sender is an HttpRequestClass
            HttpRequestClass htc = (HttpRequestClass)sender;
            // take it out of in-process list
            InProcessLock.AcquireReaderLock(-1);
            try {
                LinkedListNode<HttpRequestClass> p = InProcessRequests.First;
                while (p != null && p.Value.ReqID != htc.ReqID) p = p.Next;
                if (p != null) {
                    LockCookie lc = InProcessLock.UpgradeToWriterLock(-1);
                    try {
                        InProcessRequests.Remove(p);
                    }
                    finally {
                        InProcessLock.DowngradeFromWriterLock(ref lc);
                    }
                } else {
                    m_log.WarnFormat("[HttpScriptRequests] Finished request {0} not found in In-Process list", htc.ReqID);
                }
            }
            finally {
                InProcessLock.ReleaseReaderLock();
            }
            if (! htc.Aborted) {
                FinishedLock.AcquireWriterLock(-1);
                try {
                    FinishedRequests.AddLast(htc);
                }
                finally {
                    FinishedLock.ReleaseWriterLock();
                }
            }
        }

        private bool IsObjectThrottled(UUID ObjectID)
        {
            RequestTrackerLock.AcquireReaderLock(-1);
            try
            {
                if (RequestTracker.ContainsKey(ObjectID))
                {
                    LockCookie lc = RequestTrackerLock.UpgradeToWriterLock(-1);
                    try
                    {
                        while (RequestTracker[ObjectID].Count > 0 && (DateTime.Now - RequestTracker[ObjectID].Peek()).TotalSeconds > 20.0)
                        {
                            RequestTracker[ObjectID].Dequeue();
                        }
                    }
                    finally
                    {
                        RequestTrackerLock.DowngradeFromWriterLock(ref lc);
                    }
                    return (RequestTracker[ObjectID].Count > 24);
                }
                else
                {
                    return false;
                }
            }
            finally {
                RequestTrackerLock.ReleaseReaderLock();
            }
        }

        private void StartQueueManager()
        {
            // Are we going to use WorkManager, Watchdog, or what?
            QueueManagerLock.AcquireReaderLock(-1);
            try
            {
                if (qmThread == null)
                {
                    LockCookie lc = QueueManagerLock.UpgradeToWriterLock(-1);
                    try
                    {
                        qmThread = WorkManager.StartThread(this.QueueManagerThread, "HttpScriptRequestsQueueManager", ThreadPriority.Normal, true, true);
                    }
                    finally
                    {
                        QueueManagerLock.DowngradeFromWriterLock(ref lc);
                    }
                }
            }
            finally
            {
                QueueManagerLock.ReleaseReaderLock();
            }
        }

        private void QueueManagerThread()
        {
            DateTime LastRequestStarted = DateTime.Now;
            bool HavePending = true;
            int WorkerCount = 0;
            DateTime LastStatus = DateTime.Now;
            while (HavePending || (DateTime.Now - LastRequestStarted).TotalMilliseconds < 600000.0)
            {
                // Check if there are worker threads available
                // If there are, check if there is a queued request
                // If there is, dequeue it and start a worker thread
                // If the queue is empty, look at LastRequestStarted, and
                //      if it's been a while (maybe 10 minutes?) end the thread
                InProcessLock.AcquireReaderLock(-1);
                try
                {
                    WorkerCount = InProcessRequests.Count;
                }
                finally {
                    InProcessLock.ReleaseReaderLock();
                }
                if (WorkerCount < MaxConcurrentRequests)
                {
                    HttpRequestClass htc = null;
                    PendingLock.AcquireWriterLock(-1);
                    try {
                        LinkedListNode<HttpRequestClass> p = PendingRequests.First;
                        if (p != null) {
                            htc = p.Value;
                            PendingRequests.Remove(p);
                        } else HavePending = false;
                    }
                    catch (Exception e) {
                        m_log.ErrorFormat("[HttpScriptRequests] exception occurred dequeueing first pending request: {0}", e);
                    }
                    finally {
                        PendingLock.ReleaseWriterLock();
                    }
                    if (htc != null) {
                        // add to InProcess
                        InProcessLock.AcquireWriterLock(-1);
                        try
                        {
                            InProcessRequests.AddLast(htc);
                        }
                        finally
                        {
                            InProcessLock.ReleaseWriterLock();
                        }
                        // add a worker thread
                        //m_log.DebugFormat("[HttpScriptRequests] queueing work item for request {0}", htc.ReqID);
                        htc.RequestFinished += handleRequestFinished;
                        ThreadPool.UnsafeQueueUserWorkItem(o => htc.SendRequest(), null);
                        LastRequestStarted = DateTime.Now;
                        ++statProcessed;
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
                else
                {
                    Thread.Sleep(100);
                    HavePending = true;
                }
                /*
                // unnecessary debugging information
                if ((DateTime.Now - LastStatus).TotalSeconds >= 30.0)
                {
                    PendingLock.AcquireReaderLock(-1);
                    InProcessLock.AcquireReaderLock(-1);
                    FinishedLock.AcquireReaderLock(-1);
                    try
                    {
                        m_log.DebugFormat("[HttpScriptRequests] PendingRequests.Count = {0}", PendingRequests.Count);
                        m_log.DebugFormat("[HttpScriptRequests] InProcessRequests.Count = {0}", InProcessRequests.Count);
                        m_log.DebugFormat("[HttpScriptRequests] FinishedRequests.Count = {0}", FinishedRequests.Count);
                    }
                    finally
                    {
                        PendingLock.ReleaseReaderLock();
                        InProcessLock.ReleaseReaderLock();
                        FinishedLock.ReleaseReaderLock();
                    }
                    LastStatus = DateTime.Now;
                }
                */
                Watchdog.UpdateThread();
            }
            Watchdog.RemoveThread();
            QueueManagerLock.AcquireWriterLock(-1);
            try
            {
                qmThread = null;
            }
            finally
            {
                QueueManagerLock.ReleaseWriterLock();
            }
        }

        #region IHttpRequestModule Members

        public UUID MakeHttpRequest(string url, string parameters, string body)
        {
            return UUID.Zero;
        }

        public UUID StartHttpRequest(uint localID, UUID objectID, UUID itemID, string url, List<string> parameters, Dictionary<string, string> headers, string body)
        {
            ++statRequested;
            // This doesn't really start anything, but just queues the request as pending
            if (IsObjectThrottled(objectID)) {
                // in SL, a warning is shouted on DEBUG_CHANNEL unless HTTP_VERBOSE_THROTTLE is FALSE
                ++statThrottled;
                return UUID.Zero;
            }
            PendingLock.AcquireReaderLock(-1);
            try
            {
                if (PendingRequests.Count >= MaxQueuedRequests) {
                    ++statThrottled2;
                    return UUID.Zero;
                }
            }
            finally
            {
                PendingLock.ReleaseReaderLock();
            }

            HttpRequestClass htc = new HttpRequestClass();

            // Partial implementation: support for parameter flags needed
            //   see http://wiki.secondlife.com/wiki/LlHTTPRequest
            //
            // Parameters are expected in {key, value, ... , key, value}
            if (parameters != null)
            {
                string[] parms = parameters.ToArray();
                for (int i = 0; i < parms.Length; i += 2)
                {
                    switch (Int32.Parse(parms[i]))
                    {
                        case (int)HttpRequestConstants.HTTP_METHOD:

                            htc.HttpMethod = parms[i + 1];
                            break;

                        case (int)HttpRequestConstants.HTTP_MIMETYPE:

                            htc.HttpMIMEType = parms[i + 1];
                            break;

                        case (int)HttpRequestConstants.HTTP_BODY_MAXLENGTH:
                            htc.HttpBodyMaxLength = int.Parse(parms[i + 1]);
                            if (htc.HttpBodyMaxLength < 1) htc.HttpBodyMaxLength = 1;
                            if (htc.HttpBodyMaxLength > 16384) htc.HttpBodyMaxLength = 16384;
                            break;

                        case (int)HttpRequestConstants.HTTP_VERIFY_CERT:
                            htc.HttpVerifyCert = (int.Parse(parms[i + 1]) != 0);
                            break;

                        case (int)HttpRequestConstants.HTTP_VERBOSE_THROTTLE:

                            // TODO implement me
                            break;

                        case (int)HttpRequestConstants.HTTP_CUSTOM_HEADER:
                            //Parameters are in pairs and custom header takes
                            //arguments in pairs so adjust for header marker.
                            ++i;

                            //Maximum of 8 headers are allowed based on the
                            //Second Life documentation for llHTTPRequest.
                            for (int count = 1; count <= 8; ++count)
                            {
                                //Not enough parameters remaining for a header?
                                if (parms.Length - i < 2)
                                    break;

                                //Have we reached the end of the list of headers?
                                //End is marked by a string with a single digit.
                                //We already know we have at least one parameter
                                //so it is safe to do this check at top of loop.
                                if (Char.IsDigit(parms[i][0]))
                                    break;

                                if (htc.HttpCustomHeaders == null)
                                    htc.HttpCustomHeaders = new List<string>();

                                htc.HttpCustomHeaders.Add(parms[i]);
                                htc.HttpCustomHeaders.Add(parms[i+1]);

                                i += 2;
                            }
                            break;

                        case (int)HttpRequestConstants.HTTP_PRAGMA_NO_CACHE:
                            htc.HttpPragmaNoCache = (int.Parse(parms[i + 1]) != 0);
                            break;
                    }
                }
            }
            htc.LocalID = localID;
            htc.ObjectID = objectID;
            htc.ItemID = itemID;
            htc.Url = url;
            htc.ReqID = UUID.Random();
            htc.HttpTimeout = httpTimeout;
            htc.OutboundBody = body;
            htc.RequestHeaders = headers;
            htc.resolver = resolver;
            htc.QueuedTime = DateTime.Now;
            //htc.proxyurl = m_proxyurl;
            //htc.proxyexcepts = m_proxyexcepts;

            PendingLock.AcquireWriterLock(-1);
            try
            {
                PendingRequests.AddLast(htc);
            }
            finally
            {
                PendingLock.ReleaseWriterLock();
            }
            
            RequestTrackerLock.AcquireWriterLock(-1);
            try
            {
                if (! RequestTracker.ContainsKey(objectID)) RequestTracker.Add(objectID, new Queue<DateTime>());
                RequestTracker[objectID].Enqueue(DateTime.Now);
            }
            finally
            {
                RequestTrackerLock.ReleaseWriterLock();
            }

            StartQueueManager();

            return htc.ReqID;
        }

        public void StopHttpRequestsForScript(UUID id)
        {
            // first we remove any queued requests
            PendingLock.AcquireReaderLock(-1);
            try {
                LinkedListNode<HttpRequestClass> p = PendingRequests.First, q = null;
                while (p != null) {
                    if (p.Value.ItemID == id) {
                        LockCookie lc = PendingLock.UpgradeToWriterLock(-1);
                        try {
                            PendingRequests.Remove(p);
                            if (q == null) p = PendingRequests.First;
                            else p = q.Next;
                        }
                        catch (Exception e) {
                            m_log.ErrorFormat("[HttpScriptRequests] exception occurred removing pending request {0} for script {1}", p.Value.ReqID, id);
                            p = p.Next; // just skip it :(
                        }
                        finally {
                            PendingLock.DowngradeFromWriterLock(ref lc);
                        }
                    } else p = p.Next;
                }
            }
            finally {
                PendingLock.ReleaseReaderLock();
            }
            // now stop any in-process requests
            InProcessLock.AcquireReaderLock(-1);
            try {
                LinkedListNode<HttpRequestClass> p = InProcessRequests.First;
                while (p != null) {
                    if (p.Value.ItemID == id) {
                        p.Value.Stop();
                    }
                    p = p.Next;
                }
            }
            finally {
                InProcessLock.ReleaseReaderLock();
            }
            // may as well remove them from the Finished Queue
            FinishedLock.AcquireReaderLock(-1);
            try {
                LinkedListNode<HttpRequestClass> p = FinishedRequests.First, q = null;
                while (p != null) {
                    if (p.Value.ItemID == id) {
                        LockCookie lc = FinishedLock.UpgradeToWriterLock(-1);
                        try {
                            FinishedRequests.Remove(p);
                            if (q == null) p = FinishedRequests.First;
                            else p = q.Next;
                        }
                        catch (Exception e) {
                            m_log.ErrorFormat("[HttpScriptRequests] exception occurred removing finished request {0} for script {1}", p.Value.ReqID, id);
                            p = p.Next; // just skip it :(
                        }
                        finally {
                            FinishedLock.DowngradeFromWriterLock(ref lc);
                        }
                    } else p = p.Next;
                }
            }
            finally {
                FinishedLock.ReleaseReaderLock();
            }
            // wow, that took a long time
        }

        // I'm not sure why this isn't just DequeueNextRequest(), but it may be soon.

        public IServiceRequest GetNextCompletedRequest()
        {
            FinishedLock.AcquireReaderLock(-1);
            try
            {
                if (FinishedRequests.Count > 0) return FinishedRequests.First.Value;
            }
            finally
            {
                FinishedLock.ReleaseReaderLock();
            }
            return null;
        }

        public void RemoveCompletedRequest(UUID id)
        {
            FinishedLock.AcquireReaderLock(-1);
            try
            {
                LinkedListNode<HttpRequestClass> p = FinishedRequests.First;
                while (p != null) {
                    if (p.Value.ReqID == id) {
                        LockCookie lc = FinishedLock.UpgradeToWriterLock(-1);
                        try {
                            FinishedRequests.Remove(p);
                        }
                        finally {
                            FinishedLock.DowngradeFromWriterLock(ref lc);
                        }
                        return;
                    }
                    p = p.Next;
                }
            }
            finally {
                FinishedLock.ReleaseReaderLock();
            }
        }

        #endregion

        #region ISharedRegionModule Members

        public void Initialise(IConfigSource config)
        {
            //m_proxyurl = config.Configs["Startup"].GetString("HttpProxy");
            //m_proxyexcepts = config.Configs["Startup"].GetString("HttpProxyExceptions");
            // 
            // TODO: need to figure out what kind of formats I'm willing to parse here
            // and also what section/name to put this setting under
            // There may even be some classes that can help determine things like
            // address-belongs-to-network
            // and the List<int> part was a bad idea too, since port ranges could end
            // up being a ton of records...
              
            //
            // We should be able to just add them to a container
            // And the container should do the dirty work
            //
            // We're adding a NameResolver to the class and passing it to each HttpRequestClass
            if (config.Configs["ScriptHttp"] != null)
            {
                resolver.TryParseBlacklist(config.Configs["ScriptHttp"].GetString("ScriptHttpBlacklist"));
                MaxConcurrentRequests = config.Configs["ScriptHttp"].GetInt("MaxConcurrentRequests", MaxConcurrentRequests);
                MaxQueuedRequests = config.Configs["ScriptHttp"].GetInt("MaxQueuedRequests", MaxQueuedRequests);
            }
        }
        
        public void AddRegion(Scene scene)
        {
            scene.RegisterModuleInterface<IHttpRequestModule>(this);
        }

        public void RemoveRegion(Scene scene)
        {
            scene.UnregisterModuleInterface<IHttpRequestModule>(this);
        }

        public void PostInitialise()
        {
        }

        public void RegionLoaded(Scene scene)
        {
            scene.AddCommand("HttpRequest", this, "httprequest status", "httprequest status",
                                 "Display statistics about the state of the HttpRequest module", "",
                                 CmdStatus);
            scene.AddCommand("HttpRequest", this, "httprequest inprocess", "httprequest inprocess",
                                 "Display requests current in process for the HttpRequest module", "",
                                 CmdInProc);
        }

        public void Close()
        {
        }

        public string Name
        {
            get { return m_name; }
        }

        public Type ReplaceableInterface
        {
            get { return null; }
        }

        #endregion
        
        private void CmdStatus(string module, string[] cmd)
        {
            PendingLock.AcquireReaderLock(-1);
            try { MainConsole.Instance.OutputFormat("Pending Requests\t: {0}", PendingRequests.Count); }
            finally { PendingLock.ReleaseReaderLock(); }
            InProcessLock.AcquireReaderLock(-1);
            try { MainConsole.Instance.OutputFormat("In-Process Requests\t: {0}", InProcessRequests.Count); }
            finally { InProcessLock.ReleaseReaderLock(); }
            FinishedLock.AcquireReaderLock(-1);
            try { MainConsole.Instance.OutputFormat("Queued Responses\t: {0}", FinishedRequests.Count); }
            finally { FinishedLock.ReleaseReaderLock(); }
            QueueManagerLock.AcquireReaderLock(-1);
            try { MainConsole.Instance.OutputFormat("Queue Manager\t: {0}", qmThread == null ? "Inactive" : "Active"); }
            finally { QueueManagerLock.ReleaseReaderLock(); }
            MainConsole.Instance.OutputFormat("Total Requests\t: {0}", statRequested);
            MainConsole.Instance.OutputFormat("Total Processed\t: {0}", statProcessed);
            MainConsole.Instance.OutputFormat("Total Throttled\t: {0} (object) {1} (simulator)", statThrottled, statThrottled2);
        }

        private void CmdInProc(string module, string[] cmd)
        {
            InProcessLock.AcquireReaderLock(-1);
            try {
                MainConsole.Instance.Output(" Queue Time | Start Time | Finished | Status | Method |  Url");
                foreach (HttpRequestClass htc in InProcessRequests) {
                    MainConsole.Instance.OutputFormat(" {0,9}s | {1,9}s | {2,8} | {3,6} | {4,6} | {5}",
                        (DateTime.Now - htc.QueuedTime).TotalSeconds,
                        (DateTime.Now - htc.StartTime).TotalSeconds,
                        htc.Finished ? "TRUE" : "FALSE",
                        htc.Status, htc.HttpMethod, htc.Url);
                }
                MainConsole.Instance.OutputFormat("  Total requests in-process: {0}", InProcessRequests.Count);
            }
            finally { InProcessLock.ReleaseReaderLock(); }
        }

    }
    
    /* FilteringNameResolver:
     * parses complex blacklist strings into address/port ranges
     * does synchronous DNS lookups since it will only be called
     * by worker threads anyway :)
     * checks resulting address against blacklist
     * returns address or null
     * TODO: This class could use some cleanup/refactoring
     */
    public class FilteringNameResolver
    {
        private class FNR_Rule
        {
            private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            private class FNR_Address
            {
                public UInt32 address, netmask;
                public FNR_Address()
                {
                    address = 0;
                    netmask = 0;
                }
            }
            private List<FNR_Address> addresses = new List<FNR_Address>();
            private class FNR_PortRange
            {
                public Int32 first, last;
                public FNR_PortRange()
                {
                    first = 0;
                    last = 0;
                }
            }
            private List<FNR_PortRange> ports = new List<FNR_PortRange>();
            public FNR_Rule()
            {
            }
            public bool TryParse(string definition)
            {
                // We should be able to add things like this:
                // 192.168.0.0/24
                // 192.168.0.0/255.255.255.0
                // 85.25.207.200:9000
                // 85.25.207.200:9000-9050,12000-12050
                // 85.25.217.200
                // 65.25.30.79,65.25.72.100:80,8000-8100
                // This bitwise stuff may not be portable
                // (idk how GetAddressBytes() handles endian-ness)
                int errors = 0;
                try {
                    string[] addr_port = definition.Split(':');
                    if (addr_port.Length > 0) {
                        string[] addrs = addr_port[0].Split(',');
                        foreach (string addrdef in addrs) {
                            string[] addr_mask = addrdef.Split('/');
                            IPAddress netaddr, netmask;
                            FNR_Address naddr = new FNR_Address();
                            if (IPAddress.TryParse(addr_mask[0], out netaddr)) {
                                byte[] adBytes = netaddr.GetAddressBytes();
                                if (adBytes.Length == 4) {
                                    //naddr.address = IPAddress.HostToNetworkOrder((Int32)((adBytes[0] << 24) | (adBytes[1] << 16) | (adBytes[2] << 8) | adBytes[3]));
                                    naddr.address = (UInt32)((adBytes[0] << 24) | (adBytes[1] << 16) | (adBytes[2] << 8) | adBytes[3]);
                                    naddr.netmask = 0xffffffff;
                                    if (addr_mask.Length > 1) {
                                        if (addr_mask[1].Contains(".")) {
                                            if (IPAddress.TryParse(addr_mask[1], out netmask)) {
                                                byte[] nmBytes = netmask.GetAddressBytes();
                                                if (nmBytes.Length == 4) {
                                                    //naddr.netmask = IPAddress.HostToNetworkOrder((Int32)((nmBytes[0] << 24) | (nmBytes[1] << 16) | (nmBytes[2] << 8) | nmBytes[3]));
                                                    naddr.netmask = (UInt32)((nmBytes[0] << 24) | (nmBytes[1] << 16) | (nmBytes[2] << 8) | nmBytes[3]);
                                                }
                                            } else {
                                                // failed to parse numbers-and-dots netmask
                                                m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: error parsing numbers-and-dots notation netmask {0} in rule {1}", addr_mask[1], definition);
                                                ++errors;
                                            }
                                        } else {
                                            try {
                                                uint bits = Convert.ToUInt32(addr_mask[1]);
                                                uint mmask = 0;
                                                while (bits > 0) {
                                                    mmask = mmask << 1 | 1;
                                                    --bits;
                                                }
                                                naddr.netmask = 0xffffffff ^ mmask;
                                            }
                                            catch (Exception e) {
                                                // failed to parse integer netmask value
                                                m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: error parsing numberic netmask {0} in rule {1}", addr_mask[1], definition);
                                                ++errors;
                                            }
                                        }
                                    }
                                    // let's consider this success for address parsing
                                    addresses.Add(naddr);
                                }
                            } else {
                                // failed to parse address part
                                m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: error parsing IP address {0} in rule {1}", addr_mask[0], definition);
                                ++errors;
                            }
                        }
                    }
                    if (addr_port.Length > 1) {
                        string[] portstrs = addr_port[1].Split(',');
                        foreach (string portdef in portstrs) {
                            FNR_PortRange nport = new FNR_PortRange();
                            if (portdef.Contains("-")) {
                                // is range
                                string[] prange = portdef.Split('-');
                                try {
                                    nport.first = Convert.ToInt32(prange[0]);
                                    nport.last = Convert.ToInt32(prange[1]);
                                }
                                catch (Exception e) {
                                    // failed to parse port range
                                    m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: error parsing port range {0} in rule {1}", addr_port[1], definition);
                                    ++errors;
                                }
                            } else {
                                try {
                                    nport.first = Convert.ToInt32(portdef);
                                    nport.last = nport.first;
                                }
                                catch (Exception e) {
                                    // failed to parse port number
                                    m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: error parsing port number {0} in rule {1}", addr_port[1], definition);
                                    ++errors;
                                }
                            }
                            ports.Add(nport);
                        }
                    }
                }
                catch (Exception e) {
                    // some other exception parsing
                    m_log.ErrorFormat("[HttpScriptRequests] FilteringNameResolver rule parser: exception caught parsing rule {0}: {1}", definition, e);
                    ++errors;
                }
                return (errors == 0);
            }
            public bool Match(UInt32 compareAddr, Int32 comparePort)
            {
                bool addrMatch = false;
                foreach (FNR_Address addr in addresses) {
                    if ((addr.address & addr.netmask) == (compareAddr & addr.netmask)) {
                        addrMatch = true;
                        break;
                    }
                }
                if (addrMatch) {
                    if (ports.Count == 0) return true;
                    foreach (FNR_PortRange port in ports) {
                        if (comparePort >= port.first && comparePort <= port.last) return true;
                    }
                }
                return false;
            }
        }
        private List<FNR_Rule> Blacklist = new List<FNR_Rule>();
        public FilteringNameResolver()
        {
        }
        public bool TryParseBlacklist(string definitions)
        {
            // Since it is coming from config file, I guess we use semicolon to delimit the entries
            if (definitions == null) return true;
            string[] ruledefs = definitions.Split(';');
            foreach (string rdef in ruledefs) {
                FNR_Rule nrule = new FNR_Rule();
                if (nrule.TryParse(rdef)) Blacklist.Add(nrule);
                else return false;
            }
            return true;
        }
        public IPAddress TryResolve(string hostname, Int32 portnum)
        {
            // resolve address
            try {
                IPAddress[] addrs = Dns.GetHostAddresses(hostname);
                if (addrs.Length > 0) {
                    byte[] adBytes = addrs[0].GetAddressBytes();
                    if (adBytes.Length == 4) {
                        UInt32 addrInt = (UInt32)((adBytes[0] << 24) | (adBytes[1] << 16) | (adBytes[2] << 8) | adBytes[3]);
                        foreach (FNR_Rule rule in Blacklist) {
                            if (rule.Match(addrInt, portnum)) return null;
                        }
                        return addrs[0];
                    }
                }
            }
            catch (Exception e) {
                // fail silently
            }
            return null;
        }
    }

    public class HttpRequestClass: IServiceRequest
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        // Constants for parameters
        // public const int HTTP_BODY_MAXLENGTH = 2;
        // public const int HTTP_METHOD = 0;
        // public const int HTTP_MIMETYPE = 1;
        // public const int HTTP_VERIFY_CERT = 3;
        // public const int HTTP_VERBOSE_THROTTLE = 4;
        // public const int HTTP_CUSTOM_HEADER = 5;
        // public const int HTTP_PRAGMA_NO_CACHE = 6;
        
        private bool _finished = false;
        public bool Finished
        {
            get { return _finished; }
        }
        public int HttpBodyMaxLength = 2048;

        // Parameter members and default values
        public string HttpMethod  = "GET";
        public string HttpMIMEType = "text/plain;charset=utf-8";
        public int HttpTimeout;
        public bool HttpVerifyCert = true;
        //public bool HttpVerboseThrottle = true; // not implemented
        public List<string> HttpCustomHeaders = null;
        public bool HttpPragmaNoCache = true;
        public FilteringNameResolver resolver;
        public DateTime QueuedTime = new DateTime();
        public DateTime StartTime = new DateTime();

        // Request info
        private UUID _itemID;
        public UUID ItemID
        {
            get { return _itemID; }
            set { _itemID = value; }
        }
        private uint _localID;
        public uint LocalID
        {
            get { return _localID; }
            set { _localID = value; }
        }
        private UUID _objectID;
        public UUID ObjectID
        {
            get { return _objectID; }
            set { _objectID = value; }
        }
        //public string proxyurl;
        //public string proxyexcepts;
        public string OutboundBody = "";
        private UUID _reqID;
        public UUID ReqID
        {
            get { return _reqID; }
            set { _reqID = value; }
        }
        private bool _aborted = false;
        public bool Aborted
        {
            get { return _aborted; }
        }
        public string ResponseBody = "";
        public List<object> ResponseMetadata = new List<object>(); // only HTTP_BODY_TRUNCATED supported
        public Dictionary<string, string> RequestHeaders = new Dictionary<string, string>();
        public Dictionary<string, string> ResponseHeaders = new Dictionary<string, string>();
        public int Status = 0;
        public string Url;

        public event EventHandler RequestFinished;

        public void Process()
        {
            SendRequest();
        }
        
        public void SendRequest()
        {
            SendRequest(null);
        }

        public void SendRequest(Stack<string>redirs = null)
        {
            EventHandler handler = RequestFinished;
            try
            {
                if (redirs == null)
                {
                    redirs = new Stack<string>();
                }
                else if (redirs.Contains(Url) || redirs.Count > 19)
                {
                    ErrorResponse("Too many redirects");
                    return;
                }
                redirs.Push(Url);
                // check uri protocol
                Uri RequestUri = new Uri(Url);
                if (RequestUri.Scheme != "http" && RequestUri.Scheme != "https")
                {
                    ErrorResponse("Unsupported protocol");
                    return;
                }
                if (RequestUri.HostNameType != UriHostNameType.Dns && RequestUri.HostNameType != UriHostNameType.IPv4)
                {
                    ErrorResponse("Unsupported address type");
                    return;
                }
                Int32 RequestPort = RequestUri.Port;
                string RequestHost = RequestUri.DnsSafeHost;
                //m_log.DebugFormat("[HttpScriptRequests]: resolving host \"{0}\" for connection to port {1}", RequestHost, RequestPort);
                IPAddress RequestAddr = resolver.TryResolve(RequestHost, RequestPort);
                if (RequestAddr == null)
                {
                    ErrorResponse("Unable to resolve host or invalid address");
                    return;
                }
                //m_log.DebugFormat("[HttpScriptRequests]: resolved host \"{0}\" to address {1}", RequestHost, RequestAddr);
                string RequestPath = RequestUri.PathAndQuery;

                if (HttpMethod.ToUpper() != "GET" && HttpMethod.ToUpper() != "POST")
                {
                    ErrorResponse("Unsupported method");
                    return;
                }

                // I think it's about time to open a connection and begin the request.
                if (Aborted) {
                    if (handler != null) handler(this, EventArgs.Empty);
                    return;
                }
                //m_log.Debug("[HttpScriptRequests]: create TCP client");
                //DateTime StartTime = DateTime.Now;
                StartTime = DateTime.Now;
                TcpClient client = new TcpClient(AddressFamily.InterNetwork);
                client.ReceiveTimeout = HttpTimeout;
                client.SendTimeout = HttpTimeout / 2;
                //m_log.DebugFormat("[HttpScriptRequests]: Connecting to {0} port {1}", RequestAddr, RequestPort);
                try
                {
                    client.Connect(RequestAddr, RequestPort);
                }
                catch (Exception e)
                {
                    ErrorResponse("Failed to connect to host");
                    return;
                }
                if (client.Connected) {
                    // Now we're connected.  If HTTPS, we need to create an SSL stream
                    // Otherwise we use the regular stream
                    SslStream sSecure = null;
                    NetworkStream sPlain = client.GetStream();
                    bool UseHTTPS = false;
                    if (RequestUri.Scheme == "https") {
                        if (HttpVerifyCert)
                            sSecure = new SslStream(sPlain, true, null, null);
                        else
                            sSecure = new SslStream(sPlain, true, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
                        try
                        {
                            sSecure.AuthenticateAsClient(RequestHost);
                        }
                        catch (Exception e)
                        {
                            //m_log.DebugFormat("[HttpScriptRequests] SSL auth error exception {0}", e);
                            ErrorResponse("SSL authentication error");
                            // need to close everything up
                            sSecure.Close();
                            client.Close();
                            return;
                        }
                        UseHTTPS = true;
                    }
                    
                    // start making the header data as a string
                    string HttpHeader = string.Format("{0} {1} HTTP/1.1\r\n", HttpMethod.ToUpper(), RequestPath);
                    // add all of our standard headers to RequestHeaders
                    if (! RequestHeaders.ContainsKey("Host")) RequestHeaders.Add("Host", RequestHost);
                    if (! RequestHeaders.ContainsKey("Accept")) RequestHeaders.Add("Accept", "*/*");
                    if (! RequestHeaders.ContainsKey("Content-type")) RequestHeaders.Add("Content-type", HttpMIMEType);
                    if (! RequestHeaders.ContainsKey("Content-length")) RequestHeaders.Add("Content-length", string.Format("{0}", OutboundBody.Length));
                    if (! RequestHeaders.ContainsKey("Connection")) RequestHeaders.Add("Connection", "close");
                
                    //curlHeaders.Append("NoVerifyCert: true");
                    if (HttpPragmaNoCache && ! RequestHeaders.ContainsKey("Pragma")) RequestHeaders.Add("Pragma", "no-cache");
                    // add custom headers to RequestHeaders
                    if (HttpCustomHeaders != null)
                    {
                        for (int i = 0; i < HttpCustomHeaders.Count; i += 2)
                        {
                            if (! RequestHeaders.ContainsKey(HttpCustomHeaders[i]))
                            {
                                RequestHeaders.Add(HttpCustomHeaders[i], HttpCustomHeaders[i+1]);
                            }
                        }
                    }
                    
                    // append all RequestHeaders to the outbound header data
                    foreach (KeyValuePair<string, string> rhd in RequestHeaders)
                    {
                        HttpHeader += string.Format("{0}: {1}\r\n", rhd.Key, rhd.Value);
                    }
                    HttpHeader += "\r\n";

                    // let's send it NOW!
                    //m_log.DebugFormat("[HttpScriptRequests]: Sending request header of {0} bytes", HttpHeader.Length);
                    // should check for write timeouts
                    if (UseHTTPS)
                    {
                        sSecure.Write(Encoding.UTF8.GetBytes(HttpHeader));
                        sSecure.Flush();
                    }
                    else
                    {
                        byte[] obuf = Encoding.UTF8.GetBytes(HttpHeader);
                        sPlain.Write(obuf, 0, obuf.Length);
                        sPlain.Flush();
                    }

                    // Now do we need to send posted data?
                    if (HttpMethod.ToUpper() == "POST" && OutboundBody.Length > 0) {
                        //m_log.DebugFormat("[HttpScriptRequests]: Sending request body of {0} bytes", OutboundBody.Length);
                        // should check for write timeouts
                        if (UseHTTPS)
                        {
                            sSecure.Write(Encoding.UTF8.GetBytes(OutboundBody));
                            sSecure.Flush();
                        }
                        else
                        {
                            byte[] obuf = Encoding.UTF8.GetBytes(OutboundBody);
                            sPlain.Write(obuf, 0, obuf.Length);
                            sPlain.Flush();
                        }
                    }
                    
                    // this is really ugly.  there really should be a nicer way to handle this.
                    byte[] buffer = new byte[4096];
                    bool HaveHead = false;
                    string ResponseData = "";
                    Decoder decoder = Encoding.UTF8.GetDecoder();
                    int ExpectedLength = -1;
                    //m_log.Debug("[HttpScriptRequests]: Waiting for response");
                    while ((! Aborted) && (! Finished) && client.Connected && (DateTime.Now - StartTime).TotalMilliseconds < (double)HttpTimeout)
                    {
                        // set client receive timeout
                        client.ReceiveTimeout = HttpTimeout - (int)((DateTime.Now - StartTime).TotalMilliseconds);
                        int nbytes = 0;
                        if (UseHTTPS) nbytes = sSecure.Read(buffer, 0, 4096);
                        else nbytes = sPlain.Read(buffer, 0, 4096);
                        if (nbytes > 0)
                        {
                            //m_log.DebugFormat("[HttpScriptRequests]: Read {0} bytes", nbytes);
                            char[] chars = new char[decoder.GetCharCount(buffer, 0, nbytes)];
                            decoder.GetChars(buffer, 0, nbytes, chars, 0);
                            ResponseData += new string(chars);
                            if (! HaveHead)
                            {
                                int nlpos = ResponseData.IndexOf("\n");
                                while (nlpos != -1 && ! HaveHead)
                                {
                                    // this is a header line - extract it from the data
                                    string hln = ResponseData.Substring(0, nlpos + 1);
                                    ResponseData = ResponseData.Substring(nlpos + 1);
                                    nlpos = ResponseData.IndexOf("\n");
                                    //m_log.DebugFormat("[HttpScriptRequests]: read header line \"{0}\"", hln);
                                    if (hln.Trim().Length == 0) {
                                        // end of headers
                                        HaveHead = true;
                                        if (ResponseHeaders.ContainsKey("content-length"))
                                        {
                                            ExpectedLength = Convert.ToInt32(ResponseHeaders["content-length"]);
                                        }
                                        //m_log.DebugFormat("[HttpScriptRequests]: Finished reading header, expected content length is {0} bytes", ExpectedLength);
                                    }
                                    else
                                    {
                                        // see if it's the first line (the status)
                                        int cpos = hln.IndexOf(':');
                                        if (cpos == -1)
                                        {
                                            if (hln.Substring(0, 5) == "HTTP/")
                                            {
                                                string [] tmp = hln.Split(' ');
                                                if (tmp.Length > 1) Status = Convert.ToInt32(tmp[1]);
                                            }
                                        }
                                        else
                                        {
                                            string hdKey = hln.Substring(0, cpos).Trim().ToLower();
                                            string hdVal = hln.Substring(cpos + 1).Trim();
                                            if (! ResponseHeaders.ContainsKey(hdKey)) ResponseHeaders.Add(hdKey, hdVal);
                                        }
                                    }
                                }
                            }
                            if (HaveHead && ExpectedLength != -1 && ResponseData.Length >= ExpectedLength)
                            {
                                // we have received everything, so let's go.
                                //m_log.DebugFormat("[HttpScriptRequests]: Read {0} of {1} bytes", ResponseData.Length, ExpectedLength);
                                _finished = true;
                            }
                        }
                        else
                        {
                            // no data received - supposedly this means EOF
                            // this should stop a timed out request from returning 0/empty
                            //m_log.DebugFormat("[HttpScriptRequests]: No data received - timeout or EOF {0}", HaveHead ? " (header parsed)" : " (no header received)");
                            if (HaveHead) _finished = true;
                        }
                        if (ResponseData.Length > 100000)
                        {
                            // even with chunked encoding with chunks of 1 char, we will never accept this much data
                            //m_log.Debug("[HttpScriptRequests]: Response data reached hard limit of 100000 bytes - not reading any more");
                            _finished = true;
                        }
                    }

                    //m_log.DebugFormat("[HttpScriptRequests]: Finished reading.  Request State: Finished = {0}, Aborted = {1}, Status = {2}", Finished, Aborted, Status);
                    // let's go ahead and close up the stream and connection now
                    if (UseHTTPS) sSecure.Close();
                    sPlain.Close();
                    client.Close();

                    //m_log.DebugFormat("[HttpScriptRequests]: Closed network stream(s): Finished = {0}, Aborted = {1}, Status = {2}", Finished, Aborted, Status);
                    if (! Finished)
                    {
                        ErrorResponse("Request timed out");
                    }
                    else
                    {
                        if (Status > 299 && Status < 399 && HttpMethod.ToUpper() == "GET" && ResponseHeaders.ContainsKey("location"))
                        {
                            //m_log.DebugFormat("[HttpScriptRequests] redirected to \"{0}\" from \"{1}\"", ResponseHeaders["location"], Url);
                            Uri NewUri = new Uri(RequestUri, ResponseHeaders["location"]);
                            Url = NewUri.AbsoluteUri;
                            //m_log.DebugFormat("[HttpScriptRequests] new Url is \"{0}\"", Url);
                            _finished = false;
                            ResponseHeaders.Clear();
                            SendRequest(redirs);
                        }
                        else
                        {
                            // if i just used HTTP/1.0, i wouldn't have to worry about this.
                            // let's see if we have any problems with servers trying to force gzip on us
                            // or anything like that.  It may be useful to just change the protocol in
                            // the request if HTTP/1.1 has too many features to deal with.
                            if (ResponseHeaders.ContainsKey("transfer-encoding") && ResponseHeaders["transfer-encoding"].ToLower() == "chunked") ResponseBody = ParseChunkedData(ResponseData, Encoding.UTF8);
                            else if (ResponseHeaders.ContainsKey("transfer-coding") && ResponseHeaders["transfer-coding"].ToLower() == "chunked") ResponseBody = ParseChunkedData(ResponseData, Encoding.UTF8);
                            else ResponseBody = ResponseData;
                            if (ResponseBody.Length > HttpBodyMaxLength)
                            {
                                ResponseBody = ResponseBody.Substring(0, HttpBodyMaxLength);
                                ResponseMetadata.Add((int)HttpResponseConstants.HTTP_BODY_TRUNCATED);
                                ResponseMetadata.Add(HttpBodyMaxLength);
                            }
                            if (handler != null) handler(this, EventArgs.Empty);
                        }
                    }
                } else {
                    ErrorResponse("Failed to connect to host");
                    return;
                }
            }
            catch (Exception e)
            {
                m_log.ErrorFormat("[HttpScriptRequests]: Exception on request to {0} for {1}  {2}", Url, ItemID, e.ToString());
                ErrorResponse("Error processing request");
                return;
            }
        }
        
        private void ErrorResponse(string message, int status = (int)OSHttpStatusCode.ClientErrorJoker)
        {
            EventHandler handler = RequestFinished;
            Status = status;
            ResponseBody = message;
            _finished = true;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private static string ParseChunkedData(string data, Encoding encod)
        {
            bool parsed = (data.Length == 0);
            string ret = "";
            byte[] chunkdata = encod.GetBytes(data);
            byte[] crlf = encod.GetBytes("\r\n");
            while (!parsed)
            {
                //m_log.DebugFormat("[HttpScriptRequests] ParseChunkedData: chunkdata = \n{0}", encod.GetString(chunkdata));
                int eolpos = Array.IndexOf(chunkdata, crlf[0]);
                if (eolpos != -1)
                {
                    if (chunkdata[eolpos + 1] != crlf[1])
                        m_log.DebugFormat("[HttpScriptRequests] ParseChunkedData: CR w/o LF at {0}", eolpos);
                    int chunkpos = eolpos + 2;
                    int chunksize = Convert.ToInt32(encod.GetString(chunkdata, 0, eolpos), 16);
                    //m_log.DebugFormat("[HttpScriptRequests] ParseChunkedData: size {0}, pos {1}, len {2}", chunksize, chunkpos, chunkdata.Length);
                    if (chunksize > 0)
                    {
                        // need to get bytes, not characters...
                        byte[] chunk = new byte[chunksize];
                        Array.Copy(chunkdata, chunkpos, chunk, 0, chunksize);
                        ret += encod.GetString(chunk);
                        if (chunkdata.Length > chunkpos + chunk.Length + 2)
                        {
                            byte[] tmp = new byte[chunkdata.Length - chunk.Length - 2];
                            Array.Copy(chunkdata, chunkpos + chunksize + 2, tmp, 0, chunkdata.Length - chunkpos - chunksize - 2);
                            chunkdata = tmp;
                        }
                        else m_log.DebugFormat("[HttpScriptRequests] ParseChunkedData: chunk size of {0} specified, but only {1} characters of data remaining", chunksize, data.Length - chunkpos);
                    }
                    else parsed = true;
                }
                else
                {
                    if (chunkdata.Length > 0)
                    {
                        m_log.Debug("[HttpScriptRequests] ParseChunkedData: parse error reading chunk size, returning partial data");
                    }
                    parsed = true;
                }
                if (chunkdata.Length == 0) parsed = true;
            }
            return ret;
        }

        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // this is essentially "do not validate"
            //if (sslPolicyErrors != SslPolicyErrors.None) m_log.DebugFormat("[HttpScriptRequests] SSL policy errors: {0} -> {1}", sslPolicyErrors, sender);
            return true; //(sslPolicyErrors == SslPolicyErrors.None);
        }

        public void Stop()
        {
            m_log.DebugFormat("[HttpScriptRequests]: Stopping request to {0} for {1}  ", Url, ItemID);
            _aborted = true;
        }
    }
}
