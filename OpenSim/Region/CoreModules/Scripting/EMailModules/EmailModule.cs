/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Threading;
using DotNetOpenMail;
using DotNetOpenMail.SmtpAuth;
using log4net;
using Nini.Config;
using OpenMetaverse;
using OpenSim.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using Mono.Addins;

/*
 * The revised module will work like this:
 * an added option, IMAP_SERVER_CHECK_THROTTLE, will determine how often the IMAP server will be checked
 * for new messages.  The default value will be somewhere around 2 minutes.
 * When GetNextEmail is called, a request is added to the queue.  The request must contain the prim that
 * requested it, by UUID or LocalId, the filters for the request, ??
 * 
 * A thread is then added to the threadpool.
 * 
 * The new thread will handle the IMAP requests, if the check throttle isn't in effect, and finally check
 * the mail queue for the given object against the filters and add them to an outbound message queue.
 * 
 * Each outbound message needs to have a unique id of some kind that will be used to identify it for deletion
 * after it has been sent to the script(s) it should.
 * 
 * When GetNextEmail is called, a thread will be added to the threadpool
 *
 * This is getting a needed overhaul shortly
 * I think we can use a thread for this.
 * Keep a list of addresses we will be checking.
 * Each address has a bool registered, and a time last checked and a time of the last request.
 * When GetNextEmail is called, we check if the address is already in this list.
 *     If it's not, we add it to the list.
 * We keep a queue of requests from llGetNextEmail
 * We'll keep a bool value for is-check-thread-running
 * If this is false when adding a new address, launch the new thread which will set it to true
 *
 * The checking thread is a loop that does this
 * For each address:
 *     check if the address has been registered.  if not, register it.  if we fail, move on and log error.
 *     check if there has been a request within the maximum timeframe.  if not, delete any loaded mail for
 *          that address, unregister the address, and add to a removal list.
 *     check if the time frame for rechecking with imap has elapsed.  if so, check again.
 *     check for queued requests
 *          if matches found, dequeue the messages associated with them, and add them to the
 *              queue for results (which is picked up by AsyncCommandManager)
 *          dequeue the request
 * And then, remove any addresses in the removal list
 * If the address list is empty, set the is-check-thread-running bool to false and exit
 *
 */
namespace OpenSim.Region.CoreModules.Scripting.EmailModules {
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "EmailModule")]
    public class EmailModule : ISharedRegionModule, IEmailModule {
        //
        // Log
        //
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //
        // Module vars
        //
        private IConfigSource m_Config;
        private string m_HostName = string.Empty;
        //private string m_RegionName = string.Empty;
        private string SMTP_SERVER_HOSTNAME = string.Empty;
        private int SMTP_SERVER_PORT = 25;
        private string SMTP_SERVER_LOGIN = string.Empty;
        private string SMTP_SERVER_PASSWORD = string.Empty;
        private string IMAP_SERVER_HOSTNAME = string.Empty;
        private int IMAP_SERVER_PORT = 143;
        private string IMAP_SERVER_PASSWORD = string.Empty;
        private string EMAIL_ADDRESS_REG_URL = string.Empty;
        private int m_MaxQueueSize = 50; // maximum size of an object mail queue
        private Dictionary<UUID, List<Email>> m_MailQueues = new Dictionary<UUID, List<Email>>();
        private Dictionary<UUID, DateTime> m_LastGetEmailCall = new Dictionary<UUID, DateTime>();
        private Queue<Email> m_ResultQueue = new Queue<Email>();
        private Dictionary<UUID, DateTime> m_LastIMAPrequest = new Dictionary<UUID, DateTime>();
        private TimeSpan m_QueueTimeout = new TimeSpan(2, 0, 0); // 2 hours without llGetNextEmail drops the queue
        private TimeSpan m_ImapThrottle = new TimeSpan(0, 2, 0); // 2 minutes per imap query at most
        private string m_InterObjectHostname = "lsl.opensim.local";
        private int m_MaxEmailSize = 4096;  // largest email allowed by default, as per lsl docs.
        private List<UUID> registeredEmails = new List<UUID>();

        // Scenes by Region Handle
        private Dictionary<ulong, Scene> m_Scenes = new Dictionary<ulong, Scene>();
        private bool m_SMTP_Enabled = false;
        private bool m_IMAP_Enabled = false;

        #region ISharedRegionModule

        public void Initialise(IConfigSource config) {
            m_Config = config;
            IConfig SMTPConfig, IMAPConfig;

            //FIXME: RegionName is correct??
            //m_RegionName = scene.RegionInfo.RegionName;

            IConfig startupConfig = m_Config.Configs["Startup"];

            if (startupConfig.GetString("emailmodule", "DefaultEmailModule") == "DefaultEmailModule") {
                //Load SMTP SERVER config
                try {
                    if ((SMTPConfig = m_Config.Configs["SMTP"]) != null) {
                        m_SMTP_Enabled = SMTPConfig.GetBoolean("enabled", false);
                        if (m_SMTP_Enabled) {
                            m_HostName = SMTPConfig.GetString("host_domain_header_from", m_HostName);
                            m_InterObjectHostname = SMTPConfig.GetString("internal_object_host", m_InterObjectHostname);
                            SMTP_SERVER_HOSTNAME = SMTPConfig.GetString("SMTP_SERVER_HOSTNAME", SMTP_SERVER_HOSTNAME);
                            SMTP_SERVER_PORT = SMTPConfig.GetInt("SMTP_SERVER_PORT", SMTP_SERVER_PORT);
                            SMTP_SERVER_LOGIN = SMTPConfig.GetString("SMTP_SERVER_LOGIN", SMTP_SERVER_LOGIN);
                            SMTP_SERVER_PASSWORD = SMTPConfig.GetString("SMTP_SERVER_PASSWORD", SMTP_SERVER_PASSWORD);
                            m_MaxEmailSize = SMTPConfig.GetInt("email_max_size", m_MaxEmailSize);
                        }
                    }
                } catch (Exception e) {
                    m_log.Error("[EMAIL] SMTP not configured: " + e.Message);
                    m_SMTP_Enabled = false;
                    //return;
                }
    
                //Load IMAP SERVER config
                try {
                    if ((IMAPConfig = m_Config.Configs["IMAP"]) != null) {
                        m_IMAP_Enabled = IMAPConfig.GetBoolean("enabled", false);
                        if (m_IMAP_Enabled) {
                            IMAP_SERVER_HOSTNAME = IMAPConfig.GetString("IMAP_SERVER_HOSTNAME", IMAP_SERVER_HOSTNAME);
                            IMAP_SERVER_PORT = IMAPConfig.GetInt("IMAP_SERVER_PORT", IMAP_SERVER_PORT);
                            IMAP_SERVER_PASSWORD = IMAPConfig.GetString("IMAP_SERVER_PASSWORD", IMAP_SERVER_PASSWORD);
                            EMAIL_ADDRESS_REG_URL = IMAPConfig.GetString("EMAIL_ADDRESS_REG_URL", EMAIL_ADDRESS_REG_URL);
                            m_ImapThrottle = new TimeSpan(0, IMAPConfig.GetInt("IMAP_SERVER_CHECK_THROTTLE", (int)(m_ImapThrottle.TotalMinutes)), 0);
                        }
                    }
                } catch (Exception e) {
                    m_log.Error("[EMAIL] IMAP not configured: " + e.Message);
                    m_IMAP_Enabled = false;
                    //return;
                }
              } else {
                m_log.Error("[EMAIL] DefaultEmailModule not configured");
            }
            
        }

        public void AddRegion(Scene scene) {
            if (!m_SMTP_Enabled)
                return;

            // It's a go!
            lock (m_Scenes) {
                // Claim the interface slot
                scene.RegisterModuleInterface<IEmailModule>(this);

                // Add to scene list
                if (m_Scenes.ContainsKey(scene.RegionInfo.RegionHandle)) {
                    m_Scenes[scene.RegionInfo.RegionHandle] = scene;
                } else {
                    m_Scenes.Add(scene.RegionInfo.RegionHandle, scene);
                }
            }

            m_log.Info("[EMAIL] Activated DefaultEmailModule");
        }

        public void RemoveRegion(Scene scene) {
            lock (m_Scenes) {
                if (m_Scenes.ContainsKey(scene.RegionInfo.RegionHandle)) {
                    // let's remove any mail queues associated with this scene
                    lock (m_MailQueues) {
                        List<UUID> removal = new List<UUID>();
                        foreach (UUID objectID in m_MailQueues.Keys) {
                            if (scene.GetSceneObjectPart(objectID) != null) removal.Add(objectID);
                        }
                        foreach (UUID objectID in removal) {
                            m_log.DebugFormat("[EMAIL] Removing queue and unregistering address for object {0}", objectID.ToString());
                            m_MailQueues.Remove(objectID);
                            EmailAddressRegistrar(objectID, false);
                        }
                    }
                    m_Scenes.Remove(scene.RegionInfo.RegionHandle);
                }
                scene.UnregisterModuleInterface<IEmailModule>(this);
            }
            m_log.Info("[EMAIL] Deactivated DefaultEmailModule");
        }

        public void PostInitialise() {
        }

        public void Close() {
            m_log.Debug("[EMAIL] closing..");
        }

        public string Name {
            get { return "DefaultEmailModule"; }
        }

        public Type ReplaceableInterface {
            get { return null; }
        }

        public void RegionLoaded(Scene scene) {
        }

        #endregion

        #region IEmailModule Members
        
        public Email GetNextCompleted() {
            lock (m_ResultQueue) {
                if (m_ResultQueue.Count > 0) return m_ResultQueue.Dequeue();
                else return null;
            }
        }
        
        #endregion

        public void InsertEmail(UUID to, Email email) {
            // It's tempting to create the queue here.  Don't; objects which have
            // not yet called GetNextEmail should have no queue, and emails to them
            // should be silently dropped.

            lock (m_MailQueues) {
                if (m_MailQueues.ContainsKey(to)) {
                    if (m_MailQueues[to].Count >= m_MaxQueueSize) {
                        // fail silently
                        return;
                    }

                    lock (m_MailQueues[to]) {
                        m_MailQueues[to].Add(email);
                    }
                }
            }
        }
        
        public void ScriptRemoved(UUID scriptID) {
            lock (m_MailQueues) {
                foreach (UUID objectID in m_MailQueues.Keys) {
                    lock (m_Scenes) {
                        foreach (Scene s in m_Scenes.Values) {
                            SceneObjectPart part = s.GetSceneObjectPart(objectID);
                            if (part != null) {
                                TaskInventoryItem task = part.Inventory.GetInventoryItem(scriptID);
                                if (task != null && part.Inventory.ScriptCount() == 1) {
                                    // the part contains only one script - the one being removed
                                    m_log.DebugFormat("[EMAIL] Removing queue and unregistering address for object {0}", objectID.ToString());
                                    m_MailQueues.Remove(objectID);
                                    EmailAddressRegistrar(objectID, false);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private bool IsLocal(UUID objectID) {
            string unused;
            return (null != findPrim(objectID, out unused));
        }

        private SceneObjectPart findPrim(UUID objectID, out string ObjectRegionName) {
            lock (m_Scenes) {
                foreach (Scene s in m_Scenes.Values) {
                    SceneObjectPart part = s.GetSceneObjectPart(objectID);
                    if (part != null) {
                        ObjectRegionName = s.RegionInfo.RegionName;
                        uint localX = s.RegionInfo.WorldLocX;
                        uint localY = s.RegionInfo.WorldLocY;
                        ObjectRegionName = ObjectRegionName + " (" + localX + ", " + localY + ")";
                        return part;
                    }
                }
            }
            ObjectRegionName = string.Empty;
            return null;
        }

        private void resolveNamePositionRegionName(UUID objectID, out string ObjectName, out string ObjectAbsolutePosition, out string ObjectRegionName) {
            string m_ObjectRegionName = String.Empty;
            int objectLocX = 0;
            int objectLocY = 0;
            int objectLocZ = 0;
            ObjectName = String.Empty;
            SceneObjectPart part = findPrim(objectID, out m_ObjectRegionName);
            if (part != null) {
                objectLocX = (int)part.AbsolutePosition.X;
                objectLocY = (int)part.AbsolutePosition.Y;
                objectLocZ = (int)part.AbsolutePosition.Z;
                ObjectName = part.Name;
            }
			ObjectRegionName = m_ObjectRegionName;
			ObjectAbsolutePosition = (objectLocX == 0) // *HACK
				? String.Empty 
				: "(" + objectLocX + ", " + objectLocY + ", " + objectLocZ + ")";
        }

        /// <summary>
        /// SendEMail function utilized by llEMail
        /// </summary>
        /// <param name="objectID"></param>
        /// <param name="address"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public void SendEmail(UUID objectID, string address, string subject, string body) {
            //Check if address is empty
            if (address == string.Empty)
                return;

            //FIXED:Check the email is correct form in REGEX
            string EMailpatternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                + @"[a-zA-Z]{2,}))$";
            Regex EMailreStrict = new Regex(EMailpatternStrict);
            bool isEMailStrictMatch = EMailreStrict.IsMatch(address);
            if (!isEMailStrictMatch) {
                m_log.Error("[EMAIL] REGEX Problem in EMail Address: " + address);
                return;
            }
            if ((subject.Length + body.Length) > m_MaxEmailSize) {
                m_log.Error("[EMAIL] subject + body larger than limit of " + m_MaxEmailSize + " bytes");
                return;
            }

            string LastObjectName = string.Empty;
            string LastObjectPosition = string.Empty;
            string LastObjectRegionName = string.Empty;

            resolveNamePositionRegionName(objectID, out LastObjectName, out LastObjectPosition, out LastObjectRegionName);
            
            // we need to treat inter-region email the same as regular email.
            if (address.EndsWith(m_InterObjectHostname)) {
                // inter object email, keep it in the family
                Email email = new Email();
                email.time = ((int)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)).ToString();
                email.subject = subject;
                email.sender = objectID.ToString() + "@" + m_InterObjectHostname;
                email.message = "Object-Name: " + LastObjectName + "\nRegion: " + LastObjectRegionName + "\nLocal-Position: " + LastObjectPosition + "\n\n" + body;

                string guid = address.Substring(0, address.IndexOf("@"));
                UUID toID = new UUID(guid);

                if (IsLocal(toID)) { // TODO FIX check to see if it is local
                    // object in this region
                    InsertEmail(toID, email);
                    return; // we're done here
                }
            }
            // regular email, send it out
            try {
                //Creation EmailMessage
                EmailMessage emailMessage = new EmailMessage();
                //From
                emailMessage.FromAddress = new EmailAddress(objectID.ToString() + "@" + m_HostName);
                //To - Only One
                emailMessage.AddToAddress(new EmailAddress(address));
                //Subject
                emailMessage.Subject = subject;
                //TEXT Body
                resolveNamePositionRegionName(objectID, out LastObjectName, out LastObjectPosition, out LastObjectRegionName);
				emailMessage.BodyText = String.Empty;
				if (!String.IsNullOrEmpty(LastObjectName))
					emailMessage.BodyText += ("Object-Name: " + LastObjectName);
				if (!String.IsNullOrEmpty(LastObjectRegionName))
					emailMessage.BodyText += ("\nRegion: " + LastObjectRegionName);
				if (!String.IsNullOrEmpty(LastObjectPosition))
					emailMessage.BodyText += ("\nLocal-Position: " + LastObjectPosition + "\n\n");
				emailMessage.BodyText += body;

                //Config SMTP Server
                //Set SMTP SERVER config
                SmtpServer smtpServer = new SmtpServer(SMTP_SERVER_HOSTNAME, SMTP_SERVER_PORT);
                // Add authentication only when requested
                //
                if (SMTP_SERVER_LOGIN != String.Empty && SMTP_SERVER_PASSWORD != String.Empty) {
                    //Authentication
                    smtpServer.SmtpAuthToken = new SmtpAuthToken(SMTP_SERVER_LOGIN, SMTP_SERVER_PASSWORD);
                }
                // if email address registration is enabled, register the address here
                EmailAddressRegistrar(objectID, true);
                //Send Email Message
                emailMessage.Send(smtpServer);

                //Log
                m_log.Info("[EMAIL] EMail sent to: " + address + " from object: " + objectID.ToString() + "@" + m_HostName);
            } catch (Exception e) {
                m_log.Error("[EMAIL] DefaultEmailModule Exception: " + e.Message);
            }
        }

		/// <summary>
		/// SendOutEMail function for sending messages out of world
		/// </summary>
		/// <param name="mail"></param>
		public void SendOutEmail(OutEmail mail)
		{
			try {
				//Creation EmailMessage
				EmailMessage emailMessage = new EmailMessage();
				//From
                if (mail.senderName == "Server")
                    mail.senderName = "YrGrid";
				emailMessage.FromAddress = new EmailAddress(mail.senderID + "@" + m_HostName, mail.senderName);
				//To - Only One
				emailMessage.AddToAddress(new EmailAddress(mail.receiverAddr, mail.receiverName));
				//Subject
				emailMessage.Subject = mail.subject;
				//Body
				emailMessage.BodyText = ("From: " + mail.senderName + "\r\n\r\n" + mail.message + "\r\n");
				emailMessage.HtmlPart = new HtmlAttachment(String.Format("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><head><meta name=\"viewport\" content=\"width=device-width\" /><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head><body bgcolor=\"#f6f6f6\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;\"><table style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><td bgcolor=\"#FFFFFF\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;\"><div style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;\"><table style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><h1 style=\"font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 36px; line-height: 1.2; color: #000; font-weight: 200; margin: 40px 0 10px; padding: 0;\">Message from YrGrid</h1><p style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;\">From: {0}</p><p style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;\">{1}</p><table style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 10px 0;\"><p style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;\"><a href=\"https://yrgrid.com/\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 2; color: #FFF; text-decoration: none; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; background-color: #348eda; margin: 0 10px 0 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login to YrGrid to reply</a></p></td></tr></table></div></td></tr></table><table style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;\"><div style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;\"><table style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"><td align=\"center\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;\"></td></tr></table></div></td></tr></table></body></html>", mail.senderName, mail.message));

				//Config SMTP Server
				//Set SMTP SERVER config
				SmtpServer smtpServer = new SmtpServer(SMTP_SERVER_HOSTNAME, SMTP_SERVER_PORT);
				// Add authentication only when requested
				//
				if (SMTP_SERVER_LOGIN != String.Empty && SMTP_SERVER_PASSWORD != String.Empty) 
				{
					smtpServer.SmtpAuthToken = new SmtpAuthToken(SMTP_SERVER_LOGIN, SMTP_SERVER_PASSWORD);
				}
				// if email address registration is enabled, register the address here
				EmailAddressRegistrar(mail.senderID, true);
				//Send Email Message
				emailMessage.Send(smtpServer);

				//Log
				m_log.Info("[EMAIL] EMail sent to: " + mail.receiverAddr + " from: " + emailMessage.FromAddress.ToString());
			} catch (Exception e) {
				m_log.Error("[EMAIL] SendOutMail Exception: " + e.Message);
			}
		}

        /// <summary>
		/// GetNextEmail lol
        /// </summary>
        /// <param name="objectID"></param>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public Email GetNextEmail(UUID objectID, string sender, string subject) {
            // I'm going to create a queue for the requests,
            // put all of this into another function to be handled by a threadpool,
            // and then have the eventmanager handle the actual event rather than
            // returning the result here.
            // I can't continue blocking these script threads like this.
            Email request = new Email();
            request.sender = sender;
            request.subject = subject;
            request.recipient = objectID;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessRequestCallback), request);
            return null;
        }
        
        public void ProcessRequestCallback(object obj) {
            Email request = (Email)obj;
            UUID objectID = request.recipient;
            List<Email> queue = null;

            lock (m_LastGetEmailCall) {
                if (m_LastGetEmailCall.ContainsKey(objectID)) {
                    m_LastGetEmailCall.Remove(objectID);
                }

                m_LastGetEmailCall.Add(objectID, DateTime.Now);

                // Hopefully this isn't too time consuming.  If it is, we can always push it into a worker thread.
                DateTime now = DateTime.Now;
                List<UUID> removal = new List<UUID>();
                foreach (UUID uuid in m_LastGetEmailCall.Keys) {
                    if ((now - m_LastGetEmailCall[uuid]) > m_QueueTimeout) {
                        removal.Add(uuid);
                    }
                }

                foreach (UUID remove in removal) {
                    m_LastGetEmailCall.Remove(remove);
                    lock (m_MailQueues) {
                        m_MailQueues.Remove(remove);
                        // if email address registration is enabled, unregister the address here
                        EmailAddressRegistrar(remove, false);
                    }
                }
            }

            lock (m_MailQueues) {
                if (m_MailQueues.ContainsKey(objectID)) {
                    queue = m_MailQueues[objectID];
                }
            }

            if (queue == null) {
                lock (m_MailQueues) {
                    m_MailQueues.Add(objectID, new List<Email>());
                    queue = m_MailQueues[objectID];
                    // if email address registration is enabled, register the address here
                    EmailAddressRegistrar(objectID, true);
                }
            }
            // if IMAP is enabled, check here
            InsertMessagesFromIMAP(objectID);
            lock (queue) {
                if (queue.Count > 0) {
                    int i;

                    for (i = 0; i < queue.Count; i++) {
                        if ((string.IsNullOrEmpty(request.sender) || request.sender.Equals(queue[i].sender)) && (string.IsNullOrEmpty(request.subject) || request.subject.Equals(queue[i].subject))) {
                            break;
                        }
                    }

                    if (i != queue.Count) {
                        Email ret = queue[i];
                        queue.Remove(ret);
                        ret.numLeft = queue.Count;
                        lock (m_Scenes) {
                            foreach (Scene s in m_Scenes.Values) {
                                SceneObjectPart part = s.GetSceneObjectPart(objectID);
                                if (part != null) {
                                    ret.LocalId = part.LocalId;
                                    lock (m_ResultQueue) {
                                        m_ResultQueue.Enqueue(ret);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void InsertMessagesFromIMAP(UUID objectID) {
            bool imapThrottled = false;
            lock (m_LastIMAPrequest) {
                if (m_LastIMAPrequest.ContainsKey(objectID)) {
                    if (DateTime.Now - m_LastIMAPrequest[objectID] < m_ImapThrottle) imapThrottled = true;
                } else m_LastIMAPrequest.Add(objectID, DateTime.Now);
            }
            if (m_IMAP_Enabled && !imapThrottled) {
                // connect to IMAP server
                //m_log.Debug("[EMAIL] IMAP connecting");
                System.Net.Sockets.TcpClient imapclient = new System.Net.Sockets.TcpClient(IMAP_SERVER_HOSTNAME, IMAP_SERVER_PORT);
                if (imapclient.Connected) {
                    //m_log.Debug("[EMAIL] IMAP logging in");
                    if (IMAPlogin(imapclient, string.Format("{0}@{1}", objectID.ToString(), m_InterObjectHostname), IMAP_SERVER_PASSWORD)) {
                        //m_log.Debug("[EMAIL] IMAP selecting INBOX");
                        Int32 nmsg;
                        if (IMAPselectINBOX(imapclient, out nmsg)) {
                            //m_log.DebugFormat("[EMAIL] selected INBOX: messages: {0}", nmsg);
                            if (nmsg > 0) {
                                for (Int32 i = 1; i <= nmsg; ++i) {
                                    Email email;// = new Email();
                                    if (IMAPfetchEmail(imapclient, i, out email)) {
                                        m_log.DebugFormat("[EMAIL] IMAP fetched message {0} from {1} subject {2}", i, email.sender, email.subject);
                                        // insert email to queue
                                        InsertEmail(objectID, email);
                                        // remove from server
                                        if (!IMAPflagMessageDeleted(imapclient, i)) {
                                            m_log.WarnFormat("[EMAIL] IMAP flag message {0} deleted failed", i);
                                        }
                                    } else {
                                        m_log.WarnFormat("[EMAIL] IMAP fetch failed for message {0}", i);
                                    }
                                }
                                if (!IMAPexpunge(imapclient)) {
                                    m_log.Warn("[EMAIL] IMAP expunge failed");
                                }
                            }
                        } else {
                            m_log.WarnFormat("[EMAIL] IMAP client: failed to select INBOX for object {0}", objectID);
                        }
                        if (!IMAPlogout(imapclient)) {
                            m_log.WarnFormat("[EMAIL] IMAP logout failed for object {0}", objectID);
                        }
                        m_LastIMAPrequest[objectID] = DateTime.Now;
                    } else {
                        m_log.WarnFormat("[EMAIL] IMAP server login failed for object {0}", objectID);
                    }
                    // should we make sure it's closed?
                    imapclient.Close();
                } else {
                    m_log.Warn("[EMAIL] IMAP server connect failed");
                }
            }
        }
        
        private bool IMAPlogin(TcpClient client, string username, string password) {
            return IMAPsimpleCommand(
                client,
                string.Format("LOGIN {0} {1}", username, password)
            );
        }

        private bool IMAPselectINBOX(TcpClient client, out Int32 messages) {
            byte[] bytear = System.Text.Encoding.UTF8.GetBytes("$ SELECT INBOX \r\n");
            client.GetStream().Write(bytear, 0, bytear.Length);
            client.GetStream().Flush();
            byte[] buf = new byte[4096];
            string data = string.Empty;
            messages = 0;
            while (client.Connected) {
                int n = client.GetStream().Read(buf, 0, 4096);
                data += System.Text.Encoding.UTF8.GetString(buf, 0, n);
                int nli = data.IndexOf('\n');
                while (nli != -1) {
                    if (nli > 0) {
                        string rln = data.Substring(0, nli);
                        if (rln[0] == '$') {
                            return (rln.Substring(2, 2).ToUpper() == "OK");
                        } else if (rln[0] == '*') {
                            Match m = Regex.Match(rln, "\\* ([0-9]+) EXISTS");
                            if (m.Success) {
                                messages = Convert.ToInt32(m.Groups[1].Value);
                            }
                        }
                    }
                    data = data.Remove(0, nli + 1);
                    nli = data.IndexOf('\n');
                }
            }
            return false;
        }
        
        private bool IMAPfetchEmail(TcpClient client, Int32 num, out Email email) {
            string command = string.Format("$ FETCH {0} body[header] \r\n", num);
            byte[] bytear = System.Text.Encoding.UTF8.GetBytes(command);
            byte[] buf = new byte[4096];
            string data = string.Empty;
            string msg_head = string.Empty, msg_text = string.Empty;
            email = new Email();
            // get message header
            client.GetStream().Write(bytear, 0, bytear.Length);
            client.GetStream().Flush();
            while (client.Connected && msg_head == string.Empty) {
                int n = client.GetStream().Read(buf, 0, 4096);
                data += System.Text.Encoding.UTF8.GetString(buf, 0, n);
                int nli = data.IndexOf('\n');
                while (nli != -1) {
                    if (nli > 0) {
                        string rln = data.Substring(0, nli);
                        if (rln[0] == '$') {
                            if (rln.Substring(2, 2).ToUpper() == "OK") break;
                            else return false;
                        } else if (rln[0] == '*') {
                            Match m = Regex.Match(rln, "\\* ([0-9]+) FETCH \\(BODY\\[HEADER\\] \\{([0-9]+)\\}");
                            if (m.Success) {
                                Int32 fnum = Convert.ToInt32(m.Groups[1].Value);
                                if (fnum == num) {
                                    Int32 flen = Convert.ToInt32(m.Groups[2].Value);
                                    //m_log.DebugFormat("[EMAIL] IMAP - message {0} fetched: {1} length", fnum, flen);
                                    // now remove this line
                                    data = data.Remove(0, nli + 1);
                                    while (data.Length < flen) {
                                        n = client.GetStream().Read(buf, 0, 4096);
                                        data += System.Text.Encoding.UTF8.GetString(buf, 0, n);
                                    }
                                    msg_head = data.Substring(0, flen);
                                    data = data.Remove(0, flen);
                                    nli = data.IndexOf('\n'); // remaining should always be ")\r\n"
                                } else {
                                    // it doesn't seem like this should ever happen
                                    m_log.WarnFormat( "[EMAIL] IMAP - message {0} fetched, but expected {1}", fnum, num);
                                }
                            }
                            
                        }
                    }
                    data = data.Remove(0, nli + 1);
                    nli = data.IndexOf('\n');
                }
            }
            // get message text
            command = string.Format("$ FETCH {0} body[text] \r\n", num);
            bytear = System.Text.Encoding.UTF8.GetBytes(command);
            data = string.Empty;
            client.GetStream().Write(bytear, 0, bytear.Length);
            client.GetStream().Flush();
            while (client.Connected && msg_text == string.Empty) {
                int n = client.GetStream().Read(buf, 0, 4096);
                data += System.Text.Encoding.ASCII.GetString(buf, 0, n);
                int nli = data.IndexOf('\n');
                while (nli != -1) {
                    if (nli > 0) {
                        string rln = data.Substring(0, nli);
                        if (rln[0] == '$') {
                            if (rln.Substring(2, 2).ToUpper() == "OK")
                                break;
                            else
                                return false;
                        } else if (rln[0] == '*') {
                            Match m = Regex.Match(rln, "\\* ([0-9]+) FETCH \\(BODY\\[TEXT\\] \\{([0-9]+)\\}");
                            if (m.Success) {
                                Int32 fnum = Convert.ToInt32(m.Groups[1].Value);
                                if (fnum == num) {
                                    Int32 flen = Convert.ToInt32(m.Groups[2].Value);
                                    //m_log.DebugFormat("[EMAIL] IMAP - message {0} fetched: {1} length", fnum, flen);
                                    // now remove this line
                                    data = data.Remove(0, nli + 1);
                                    while (data.Length < flen) {
                                        n = client.GetStream().Read(buf, 0, 4096);
                                        data += System.Text.Encoding.UTF8.GetString(buf, 0, n);
                                    }
                                    msg_text = data.Substring(0, flen);
                                    data = data.Remove(0, flen);
                                    nli = data.IndexOf('\n'); // remaining should always be ")\r\n"
                                } else {
                                    // it doesn't seem like this should ever happen
                                    m_log.WarnFormat("[EMAIL] IMAP - message {0} fetched, but expected {1}", fnum, num);
                                }
                            }
                            
                        }
                    }
                    data = data.Remove(0, nli + 1);
                    nli = data.IndexOf('\n');
                }
            }
            email = new Email();
            string[] headerLines = msg_head.Split(new char[]{'\r', '\n'});
            foreach (string headLine in headerLines) {
                if (headLine != string.Empty) {
                    if (headLine.Substring(0, 5).ToUpper() == "FROM:") {
                        email.sender = headLine.Substring(5).Trim();
                    } else if (headLine.Substring(0, 5).ToUpper() == "TIME:") {
                        email.time = headLine.Substring(5).Trim();
                    } else if (headLine.Substring(0, 8).ToUpper() == "SUBJECT:") {
                        email.subject = headLine.Substring(8).Trim();
                    } // ignore all other headers, I guess
                }
            }
            if (msg_text.Length > m_MaxEmailSize) msg_text.Remove(m_MaxEmailSize);
            email.message = msg_text;
            return true;
        }
        
        private bool IMAPflagMessageDeleted(TcpClient client, Int32 num) {
            return IMAPsimpleCommand(client, string.Format("STORE {0} +FLAGS (\\Deleted)", num));
        }
        
        private bool IMAPexpunge(TcpClient client) {
            return IMAPsimpleCommand(client, string.Format("EXPUNGE"));
        }

        private bool IMAPlogout(TcpClient client) {
            IMAPsimpleCommand(client, string.Format("LOGOUT"));
            return true;
        }
        
        private bool IMAPsimpleCommand(TcpClient client, string cmd) {
            string command = string.Format("$ {0} \r\n", cmd);
            byte[] bytear = System.Text.Encoding.UTF8.GetBytes(command);
            client.GetStream().Write(bytear, 0, bytear.Length);
            client.GetStream().Flush();
            byte[] buf = new byte[4096];
            string data = string.Empty;
            while (client.Connected) {
                int n = client.GetStream().Read(buf, 0, 4096);
                //m_log.DebugFormat("[EMAIL] IMAPsimpleCommand(client, \"{0}\"): read {1} bytes", cmd, n);
                if (n > 0) {
                    data += System.Text.Encoding.UTF8.GetString(buf, 0, n);
                    int nli = data.IndexOf('\n');
                    while (nli != -1) {
                        if (nli > 0) {
                            string rln = data.Substring(0, nli);
                            //m_log.DebugFormat("[EMAIL] IMAPsimpleCommand process line: {0}", rln);
                            if (rln[0] == '$') {
                                return (rln.Substring(2, 2).ToUpper() == "OK");
                            } else if (rln[0] == '*') {
                                if (rln.Substring(2, 3).ToUpper() == "BYE") return false;
                            }
                        //} else {
                            //m_log.Debug("[EMAIL] IMAPsimpleCommand empty line");
                        }
                        data = data.Remove(0, nli + 1);
                        nli = data.IndexOf('\n');
                    }
                }
            }
            return false;
        }
        
        private void EmailAddressRegistrar(UUID objectID, bool regist) {
            // if we kept a list of all registered addresses here, we could avoid making
            // multiple requests at the expense of memory usage
            if (EMAIL_ADDRESS_REG_URL == string.Empty) return;
            if (regist != registeredEmails.Contains(objectID)) {
                string[] parts = EMAIL_ADDRESS_REG_URL.Split('/');
                if (parts[0] == "http:" && string.IsNullOrEmpty(parts[1])) {
                    string hostpart = parts[2];
                    Int32 port = 80;
                    if (hostpart.Contains(":")) {
                        string[] temp = hostpart.Split(':');
                        hostpart = temp[0];
                        port = Convert.ToInt32(temp[1]);
                    }
                    System.Net.Sockets.TcpClient htc = new System.Net.Sockets.TcpClient(hostpart, port);
                    if (htc.Connected) {
                        System.Net.Sockets.NetworkStream hts = htc.GetStream();
                        string poststr = string.Format("act={0}&address={1}%40{2}&password={3}", regist ? "register" : "unregister", objectID.ToString(), m_InterObjectHostname, IMAP_SERVER_PASSWORD);
                        string docpath = string.Join("/", parts, 3, parts.Length - 3);
                        string reqstr = string.Format("POST /{0} HTTP/1.1\nHost: {1}\nContent-type: application/x-www-form-urlencoded\nContent-length: {2}\nConnection: close\n\n{3}", docpath, hostpart, poststr.Length, poststr);
                        byte[] reqdat = System.Text.Encoding.UTF8.GetBytes(reqstr);
                        hts.Write(reqdat, 0, reqdat.Length);
                        byte[] htb = new byte[4096];
                        int n = hts.Read(htb, 0, 4096);
                        if (n > 0) {
                            string resp = System.Text.Encoding.UTF8.GetString(htb, 0, n);
                            if (resp.Substring(0, 15).ToUpper() == "HTTP/1.1 200 OK") {
                                m_log.DebugFormat("[EMAIL] {0} address for object {1}", regist ? "Registered" : "Unregistered", objectID.ToString());
                                if (regist) registeredEmails.Add(objectID);
                                else registeredEmails.Remove(objectID);
                            } else {
                                m_log.DebugFormat("[EMAIL] EmailAddressRegistrar failed request: {0}", resp.Split('\n')[0]);
                            }
                        } else m_log.Debug("[EMAIL] EmailAddressRegistrar: empty response or connection closed");
                        hts.Close();
                        hts.Dispose();
                        htc.Close();
                    } else {
                        m_log.WarnFormat("[EMAIL] EmailAddressRegistrar: failed to connect to address {0} port {1}", hostpart, port);
                    }
                } else {
                    m_log.Warn("[EMAIL] EmailAddressRegistrar only supports http at this time");
                }
            }
        }
        
        public static int ignoreDataFromCurl(byte[] buf, int size, int nmemb, object extraData) {
            return nmemb * size;
        }
        
    }
}
