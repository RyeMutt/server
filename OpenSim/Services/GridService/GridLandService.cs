﻿/** 
 * Copyright (C) 2015, Cinder Roxley <cinder@sdf.org>
 * 
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 * 
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using Nini.Config;
using log4net;
using OpenSim.Framework;
using OpenSim.Server.Base;
using OpenSim.Services.Interfaces;
using OpenMetaverse;

namespace OpenSim.Services.GridService
{
    public class GridLandService : LandServiceBase, IGridLandService
	{
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		protected IUserPremiumService m_UserPremiumService;

        public GridLandService(IConfigSource config) : base(config)
		{
            m_log.DebugFormat("[LAND SERVICE]: Starting...");
            IConfig landConfig = config.Configs["GridLandService"];

            if (landConfig != null)
            {
				string premiumServiceDll = landConfig.GetString("UserPremiumService", string.Empty);
				if (premiumServiceDll != string.Empty)
					m_UserPremiumService = LoadPlugin<IUserPremiumService>(premiumServiceDll, new Object[] { config });
            }
		}

        public int GetAgentLandArea(UUID agentID)
        {
            return m_Database.GetAgentLandArea(agentID);
        }

        public LandCreditStruct GetLandCredit(UUID agentID)
        {
            LandCreditStruct landData = new LandCreditStruct(0, 0);
            landData.Credit = m_UserPremiumService.GetAgentLandCredit(agentID);
            landData.Committed = m_Database.GetAgentLandArea(agentID);
            return landData;
        }
	}
}

