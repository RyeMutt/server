/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
using OpenSim.Framework;
using log4net;
using MySql.Data.MySqlClient;
using OpenMetaverse;

namespace OpenSim.Data.MySQL
{
    public class MySQLFSAssetData : IFSAssetDataPlugin
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected string m_ConnectionString;
        protected string m_Table;
        protected object m_connLock = new object();

        /// <summary>
        /// Number of days that must pass before we update the access time on an asset when it has been fetched
        /// Config option to change this is "DaysBetweenAccessTimeUpdates"
        /// </summary>
        private int DaysBetweenAccessTimeUpdates = 0;

        protected virtual Assembly Assembly => GetType().Assembly;

        #region IPlugin Members

        public string Version => "1.0.0.0";

        // Loads and initialises the MySQL storage plugin and checks for migrations
        public void Initialise(string connect, string realm, int updateAccessTime)
        {
            m_ConnectionString = connect;
            m_Table = realm;

            DaysBetweenAccessTimeUpdates = updateAccessTime;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
                {
                    connection.Open();

                    Migration m = new Migration(connection, Assembly, "FSAssetStore");
                    m.Update();
                }
            }
            catch (MySqlException e)
            {
                m_log.ErrorFormat("[ASSETS]: Can't connect to database: {0}", e.Message);
            }
        }

        public void Initialise()
        {
            throw new NotImplementedException();
        }

        public void Dispose() { }

        public string Name => "MySQL FSAsset storage engine";

        #endregion

        #region IFSAssetDataPlugin Members

        public AssetMetadata Get(string id, out string hash)
        {
            hash = String.Empty;
            AssetMetadata meta;
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                using (
                    MySqlCommand cmd =
                        new MySqlCommand(
                            $"select id, name, description, type, hash, create_time, access_time, asset_flags from {m_Table} where id = ?id",
                            connection))
                {
                    cmd.Parameters.AddWithValue("?id", id);

                    connection.Open();
                    int accessTime;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            return null;
                        }

                        meta = new AssetMetadata();

                        hash = reader["hash"].ToString();

                        meta.ID = id;
                        meta.FullID = new UUID(id);
                        meta.Name = reader["name"].ToString();
                        meta.Description = reader["description"].ToString();
                        meta.Type = (sbyte)Convert.ToInt32(reader["type"]);
                        meta.ContentType = SLUtil.SLAssetTypeToContentType(meta.Type);
                        meta.CreationDate = Util.ToDateTime(Convert.ToInt32(reader["create_time"]));
                        meta.Flags = (AssetFlags)Convert.ToInt32(reader["asset_flags"]);

                        accessTime = Convert.ToInt32(reader["access_time"]);
                    }

                    if (DaysBetweenAccessTimeUpdates > 0 &&
                        (DateTime.UtcNow - Utils.UnixTimeToDateTime(accessTime)).TotalDays >
                        DaysBetweenAccessTimeUpdates)
                    {
                        cmd.CommandText = $"UPDATE {m_Table} SET `access_time` = UNIX_TIMESTAMP() WHERE `id` = ?id";

                        cmd.ExecuteNonQuery();
                    }
                }
            }

            return meta;
        }

        public bool Store(AssetMetadata meta, string hash)
        {
            try
            {
                string oldhash;
                AssetMetadata existingAsset = Get(meta.ID, out oldhash);
                if (existingAsset != null)
                    return false;
                using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
                {
                    var query = $"insert into {m_Table} (id, name, description, type, hash, asset_flags, create_time, access_time) values ( ?id, ?name, ?description, ?type, ?hash, ?asset_flags, UNIX_TIMESTAMP(), UNIX_TIMESTAMP())";

                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("?id", meta.ID);
                        cmd.Parameters.AddWithValue("?name", meta.Name);
                        cmd.Parameters.AddWithValue("?description", meta.Description);
                        cmd.Parameters.AddWithValue("?type", meta.Type.ToString());
                        cmd.Parameters.AddWithValue("?hash", hash);
                        cmd.Parameters.AddWithValue("?asset_flags", meta.Flags);

                        try
                        {
                            connection.Open();
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                return true;
                            }
                        }
                        catch (MySqlException)
                        {
                            m_log.ErrorFormat("[ASSETS] MySQL command: {0}", cmd.CommandText);
                            throw;
                        }

                        //cmd.CommandText = String.Format("update {0} set hash = ?hash, access_time = UNIX_TIMESTAMP() where id = ?id", m_Table);

                        //ExecuteNonQuery(cmd);
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                m_log.Error("[FSAssets] Failed to store asset with ID " + meta.ID);
                m_log.Error(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Check if the assets exist in the database.
        /// </summary>
        /// <param name="uuids">The asset UUID's</param>
        /// <returns>For each asset: true if it exists, false otherwise</returns>
        public bool[] AssetsExist(UUID[] uuids)
        {
            if (uuids.Length == 0)
                return new bool[0];

            HashSet<UUID> exists = new HashSet<UUID>();

            string ids = "'" + string.Join("','", uuids) + "'";
            string sql = string.Format("select id from {1} where id in ({0})", ids, m_Table);
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                {
                    connection.Open();
                    using (MySqlDataReader dbReader = cmd.ExecuteReader())
                    {
                        while (dbReader.Read())
                        {
                            UUID id = DBGuid.FromDB(dbReader["ID"]);
                            exists.Add(id);
                        }
                    }
                }
            }

            bool[] results = new bool[uuids.Length];
            for (int i = 0; i < uuids.Length; i++)
                results[i] = exists.Contains(uuids[i]);
            return results;
        }

        public int Count()
        {
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand($"select count(*) as count from {m_Table}", connection))
                {
                    connection.Open();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return Convert.ToInt32(reader["count"]);
                        }
                    }
                }
            }
            return -1;
        }

        public bool Delete(string id)
        {
            using (MySqlConnection connection = new MySqlConnection(m_ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand($"delete from {m_Table} where id = ?id", connection))
                {
                    cmd.Parameters.AddWithValue("?id", id);

                    try
                    {
                        connection.Open();
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            return true;
                        }
                    }
                    catch (MySqlException)
                    {
                        m_log.ErrorFormat("[ASSETS] MySQL command: {0}", cmd.CommandText);
                        throw;
                    }
                }
            }
            return false;
        }

        public void Import(string conn, string table, int start, int count, bool force, FSStoreDelegate store)
        {
            try
            {
                int imported;
                using (MySqlConnection importConn = new MySqlConnection(conn))
                {
                    imported = 0;
                    using (MySqlCommand cmd = importConn.CreateCommand())
                    {
                        string limit = string.Empty;
                        if (count != -1)
                        {
                            limit = $" limit {start},{count}";
                        }

                        cmd.CommandText = $"select * from {table}{limit}";

                        MainConsole.Instance.Output("Querying database");
                        importConn.Open();
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            MainConsole.Instance.Output("Reading data");

                            while (reader.Read())
                            {
                                if ((imported % 100) == 0)
                                {
                                    MainConsole.Instance.Output($"{imported} assets imported so far");
                                }

                                AssetBase asset = new AssetBase();
                                AssetMetadata meta = new AssetMetadata();

                                meta.ID = reader["id"].ToString();
                                meta.FullID = new UUID(meta.ID);

                                meta.Name = reader["name"].ToString();
                                meta.Description = reader["description"].ToString();
                                meta.Type = (sbyte)Convert.ToInt32(reader["assetType"]);
                                meta.ContentType = SLUtil.SLAssetTypeToContentType(meta.Type);
                                meta.CreationDate = Util.ToDateTime(Convert.ToInt32(reader["create_time"]));

                                asset.Metadata = meta;
                                asset.Data = (byte[])reader["data"];

                                store(asset, force);

                                imported++;
                            }
                        }
                    }
                }

                MainConsole.Instance.Output($"Import done, {imported} assets imported");
            }
            catch (MySqlException e)
            {
                m_log.ErrorFormat("[ASSETS]: Can't connect to database: {0}",
                        e.Message);
            }
        }

        #endregion
    }
}
