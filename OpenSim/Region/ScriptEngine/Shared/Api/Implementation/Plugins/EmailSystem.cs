/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.CoreModules.Scripting.EmailModules;
using OpenSim.Region.ScriptEngine.Shared;
using OpenSim.Region.ScriptEngine.Interfaces;
using OpenSim.Region.ScriptEngine.Shared.Api;
using OpenMetaverse;

namespace OpenSim.Region.ScriptEngine.Shared.Api.Plugins
{
    public class EmailSystem
    {
        public AsyncCommandManager m_CmdManager;

        public EmailSystem(AsyncCommandManager CmdManager)
        {
            m_CmdManager = CmdManager;
        }

        public void CheckEmailRequests()
        {
            if (m_CmdManager.m_ScriptEngine.World == null)
                return;
            
            // check for completed mail checks and post script events for them
            // this contains the bulk of what was previously part of GetNextEmail
            
            IEmailModule iEmailMod = m_CmdManager.m_ScriptEngine.World.RequestModuleInterface<IEmailModule>();
            
            // need to get the task uuid
            
            Email email = null;
            
            if (iEmailMod != null) email = iEmailMod.GetNextCompleted();

            while (email != null)
            {

                //iEmailMod.RemoveCompleted(email);

                object[] resobj = new object[]
                {
                                new LSL_Types.LSLString(email.time),
                                new LSL_Types.LSLString(email.sender),
                                new LSL_Types.LSLString(email.subject),
                                new LSL_Types.LSLString(email.message),
                                new LSL_Types.LSLInteger(email.numLeft)
                };

                foreach (IScriptEngine e in m_CmdManager.ScriptEngines)
                {
                    if (e.PostObjectEvent(email.LocalId,
                            new EventParams("email",
                            resobj, new DetectParams[0])))
                        break; // idk if we really want to break here
                }
                email = iEmailMod.GetNextCompleted();
            }
        }
    }
}
