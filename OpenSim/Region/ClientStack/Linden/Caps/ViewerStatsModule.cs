/*
 * Copyright (c) Contributors, http://opensimulator.org/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the OpenSimulator Project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using log4net;
using Mono.Addins;
using Nini.Config;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using OpenSim.Framework.Servers.HttpServer;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using OpenSim.Region.PhysicsModules.SharedBase;
using OpenSim.Services.Interfaces;
using Caps = OpenSim.Framework.Capabilities.Caps;
using OpenSim.Capabilities.Handlers;

namespace OpenSim.Region.ClientStack.Linden
{
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "ViewerStatsModule")]
    public class ViewerStatsModule : ISharedRegionModule
    {
        //private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private List<Scene> m_scenes = new List<Scene>();
        private IUserLogService m_UserLogService = null;

        public void Initialise(IConfigSource source)
        {

        }

        #region Region module

        public void AddRegion(Scene scene)
        {
            lock (m_scenes) m_scenes.Add(scene);
        }

        public void RemoveRegion(Scene scene)
        {
            lock (m_scenes) m_scenes.Remove(scene);
            scene.EventManager.OnRegisterCaps -= RegisterCaps;
            scene = null;
        }

        public void RegionLoaded(Scene scene)
        {
            m_UserLogService = m_scenes[0].RequestModuleInterface<IUserLogService>();
            scene.EventManager.OnRegisterCaps += delegate(UUID agentID, OpenSim.Framework.Capabilities.Caps caps)
                {
                    RegisterCaps(agentID, caps);
                };
        }

        public void PostInitialise() {}

        public void Close() {}

        public string Name 
        { 
            get { return "ViewerStatsModule"; } 
        }

        public Type ReplaceableInterface
        {
            get { return null; }
        }

        #endregion Region module

        public void RegisterCaps(UUID agent, Caps caps)
        {
            UUID capId = UUID.Random();
            caps.RegisterHandler("ViewerStats",
                new RestStreamHandler("POST", "/CAPS/" + capId,
                    delegate(string request, string path, string param,
                        IOSHttpRequest httpRequest, IOSHttpResponse httpResponse)
                    {
                        return RecordViewerStats(request, path, param, agent);
                    }));
        }

        private string RecordViewerStats(string request, string path, string param, UUID agent)
        {
            // Don't worry about deserializing this yet. We'd just have to serialize it again to send it 
            // up to R.O.B.U.S.T.
            if (m_UserLogService != null)
                m_UserLogService.RecordViewerStats(agent, request);
            // Viewer doesn't expect a reply...
            return "";
        }
    }
}

